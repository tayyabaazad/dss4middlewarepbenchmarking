# MEAN Stack Decision Support System

## Features
1. On the basis of different criteria, get results to support decision making
2. Admin login
2. Add, delete update entities in the system if logged in as admin (admin authentication and autherisation)

## Installation
1. Download the repository
2. Install npm modules: `npm install` from root directory
3. Install bower modules: `bower install` from root directory

## How to seed?
1. Check the db connecting url in config/db, which could be local or online
2. Go through the seed file and confirm the content is correct
3. Run `node seed`

## How to run and use the application?
1. Once all the above steps are completed run `node server`
2. Open a browser
3. Point to http://localhost:8080 in address bar
4. Use :)