module.exports = function(app) {

    var ApplicationScenarioController = app.controllers.ApplicationScenario;
    var BenchmarkController = app.controllers.Benchmark;
    var HardwareController = app.controllers.Hardware;
    var MessageController = app.controllers.Message;
    var MessageRatioController = app.controllers.MessageRatio;
    var MessagingStyleController = app.controllers.MessagingStyle;
    var MetricController = app.controllers.Metric;
    var PerformanceController = app.controllers.Performance;
    var RoleController = app.controllers.Role;
    var ScalabilityController = app.controllers.Scalability;
    var TransactionController = app.controllers.Transaction;
    var TransactionStyleController = app.controllers.TransactionStyle;
    var WorkloadController = app.controllers.Workload;
    var MiddlewareController = app.controllers.Middleware;
    var TagController = app.controllers.Tag;




    // restful end points. It's according to the restful convention i.e. a resource/{{its ID}}/another resource/{its ID}} and so on
    app.get('/api/middlewares', MiddlewareController.Middlewares);
    app.get('/api/middlewares/:id', MiddlewareController.Middleware);

    app.post('/api/middlewares', MiddlewareController.AddMiddleware);
    app.put('/api/middlewares/:id', MiddlewareController.UpdateMiddleware);
    app.delete('/api/middlewares/:id', MiddlewareController.DeleteMiddleware);

    app.get('/api/middlewares/:mwID/application-scenarios', MiddlewareController.MiddlewareApplicationScenarios);

    app.get('/api/middlewares/:mwID/benchmarks', MiddlewareController.MiddlewareBenchmarks);

    app.get('/api/middlewares/:mwID/hardwares', MiddlewareController.MiddlewareHardwares);

    app.get('/api/middlewares/:mwID/messages', MiddlewareController.MiddlewareMessages);

    app.get('/api/middlewares/:mwID/message-ratios', MiddlewareController.MiddlewareMessageRatios);

    app.get('/api/middlewares/:mwID/messaging-styles', MiddlewareController.MiddlewareMessagingStyles);

    app.get('/api/middlewares/:mwID/metrics', MiddlewareController.MiddlewareMetrics);

    app.get('/api/middlewares/:mwID/performances', MiddlewareController.MiddlewarePerformances);

    app.get('/api/middlewares/:mwID/roles', MiddlewareController.MiddlewareRoles);

    app.get('/api/middlewares/:mwID/scalabilities', MiddlewareController.MiddlewareScalabilities);

    app.get('/api/middlewares/:mwID/transactions', MiddlewareController.MiddlewareTransactions);

    app.get('/api/middlewares/:mwID/transaction-styles', MiddlewareController.MiddlewareTransactionStyles);

    app.get('/api/middlewares/:mwID/workloads', MiddlewareController.MiddlewareWorkloads);


    app.get('/api/benchmarks', BenchmarkController.Benchmarks);
    app.get('/api/benchmarks/:id', BenchmarkController.Benchmark);
    app.post('/api/benchmarks', BenchmarkController.AddBenchmark);
    app.put('/api/benchmarks/:id', BenchmarkController.UpdateBenchmark);
    app.delete('/api/benchmarks/:id', BenchmarkController.DeleteBenchmark);
    app.get('/api/benchmarks/:id/transaction-styles', BenchmarkController.BenchmarkTransactionStyles);



    app.get('/api/application-scenarios', ApplicationScenarioController.ApplicationScenarios);
    app.get('/api/application-scenarios/:id', ApplicationScenarioController.ApplicationScenario);
    app.post('/api/application-scenarios', ApplicationScenarioController.AddApplicationScenario);
    app.put('/api/application-scenarios/:id', ApplicationScenarioController.UpdateApplicationScenario);
    app.delete('/api/application-scenarios/:id', ApplicationScenarioController.DeleteApplicationScenario);

    app.get('/api/application-scenarios/:asID/roles', ApplicationScenarioController.ApplicationScenarioRoles);

    app.get('/api/hardwares', HardwareController.Hardwares);
    app.get('/api/hardwares/:id', HardwareController.Hardware);
    app.post('/api/hardwares', HardwareController.AddHardware);
    app.put('/api/hardwares/:id', HardwareController.UpdateHardware);
    app.delete('/api/hardwares/:id', HardwareController.DeleteHardware);


    app.get('/api/messages', MessageController.Messages);
    app.get('/api/messages/:id', MessageController.Message);
    app.post('/api/messages', MessageController.AddMessage);
    app.put('/api/messages/:id', MessageController.UpdateMessage);
    app.delete('/api/messages/:id', MessageController.DeleteMessage);


    app.get('/api/message-ratios', MessageRatioController.MessageRatios);
    app.get('/api/message-ratios/:id', MessageRatioController.MessageRatio);
    app.post('/api/message-ratios', MessageRatioController.AddMessageRatio);
    app.put('/api/message-ratios/:id', MessageRatioController.UpdateMessageRatio);
    app.delete('/api/message-ratios/:id', MessageRatioController.DeleteMessageRatio);


    app.get('/api/messaging-styles', MessagingStyleController.MessagingStyles);
    app.get('/api/messaging-styles/:id', MessagingStyleController.MessagingStyle);
    app.post('/api/messaging-styles', MessagingStyleController.AddMessagingStyle);
    app.put('/api/messaging-styles/:id', MessagingStyleController.UpdateMessagingStyle);
    app.delete('/api/messaging-styles/:id', MessagingStyleController.DeleteMessagingStyle);


    app.get('/api/metrics', MetricController.Metrics);
    app.get('/api/metrics/:id', MetricController.Metric);
    app.post('/api/metrics', MetricController.AddMetric);
    app.put('/api/metrics/:id', MetricController.UpdateMetric);
    app.delete('/api/metrics/:id', MetricController.DeleteMetric);


    app.get('/api/performances', PerformanceController.Performances);
    app.get('/api/performances/:id', PerformanceController.Performance);
    app.post('/api/performances', PerformanceController.AddPerformance);
    app.put('/api/performances/:id', PerformanceController.UpdatePerformance);
    app.delete('/api/performances/:id', PerformanceController.DeletePerformance);

    app.get('/api/roles', RoleController.Roles);
    app.get('/api/roles/:id', RoleController.Role);
    app.post('/api/roles', RoleController.AddRole);
    app.put('/api/roles/:id', RoleController.UpdateRole);
    app.delete('/api/roles/:id', RoleController.DeleteRole);

    app.get('/api/scalabilities', ScalabilityController.Scalabilities);
    app.get('/api/scalabilities/:id', ScalabilityController.Scalability);
    app.post('/api/scalabilities', ScalabilityController.AddScalability);
    app.put('/api/scalabilities/:id', ScalabilityController.UpdateScalability);
    app.delete('/api/scalabilities/:id', ScalabilityController.DeleteScalability);

    app.get('/api/transactions', TransactionController.Transactions);
    app.get('/api/transactions/:id', TransactionController.Transaction);
    app.post('/api/transactions', TransactionController.AddTransaction);
    app.put('/api/transactions/:id', TransactionController.UpdateTransaction);
    app.delete('/api/transactions/:id', TransactionController.DeleteTransaction);

    app.get('/api/transaction-styles', TransactionStyleController.TransactionStyles);
    app.get('/api/transaction-styles/:id', TransactionStyleController.TransactionStyle);
    app.post('/api/transaction-styles', TransactionStyleController.AddTransactionStyle);
    app.put('/api/transaction-styles/:id', TransactionStyleController.UpdateTransactionStyle);
    app.delete('/api/transaction-styles/:id', TransactionStyleController.DeleteTransactionStyle);

    app.get('/api/workloads', WorkloadController.Workloads);
    app.get('/api/workloads/:id', WorkloadController.Workload);
    app.post('/api/workloads', WorkloadController.AddWorkload);
    app.put('/api/workloads/:id', WorkloadController.UpdateWorkload);
    app.delete('/api/workloads/:id', WorkloadController.DeleteWorkload);
    
    
    app.get('/api/tags', TagController.Tags);
    app.post('/api/tags', TagController.AddTag);

    app.post('/sign-in', function(req, res){

        if(req.body.email == "admin@dss.com" && req.body.password == "dss"){
            req.session.admin = {
                userName : "DSS Admin",
                email : "admin@dss.com"
            }

            res.status(200).send(req.session.admin)
        }
        else{
            res.status(403).send("Invalid credentials")
        }
    });

    app.get('/sign-out', function(req, res){

        req.session.admin = undefined;
        res.status(200).send("success")

    });


    app.get('/signed-in-user', function(req, res){
        if(req.session.admin){
            res.status(200).send(req.session.admin)
        }
        else{
            res.status(403).send(null)
        }
    });



    app.get('*', function(req, res) {
        res.sendfile('./public/index.html');
    });
};