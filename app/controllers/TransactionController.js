module.exports = function(app) {
    var Transaction = require('../models/Transaction');

    var Controller = {
        name: 'Transaction'
    }

    Controller.Transactions = function(req, res) {
        var fields = req.query.fields? req.query.fields.join(' ') : '';
        Transaction.find({}, fields, function (err, docs) {
            if(err){
                res.status(500).send(err);
            }
            else {
                if(req.query.populate == '1'){
                    Transaction.populate(docs, { path: 'middleware', model: 'Middleware', select: 'name -_id' }, function(err, docs) {
                        if(err){
                            res.status(500).send(err)
                        }
                        else{
                            res.send(docs)
                        }
                    });
                }
                else{
                    res.send(docs);
                }

            }
        })
    }


    Controller.Transaction = function(req, res) {
        Transaction.findOne({_id: req.params.id}, function (err, doc) {
            if(err){
                res.status(500).send(err);
            }
            else if(doc){
                res.send(doc);
            }
            else{
                res.status(404).send(null)
            }
        })
    }


    Controller.AddTransaction = function(req, res) {
        Transaction.create(req.body, function (err, doc) {
            if(err){
                res.status(500).send(err);
            }
            else{
                res.send(doc)
            }
        })
    }


    Controller.UpdateTransaction = function(req, res) {
        Transaction.findOneAndUpdate({_id: req.params.id}, req.body, {new: true}, function (err, doc) {
            if(err){
                res.status(500).send(err);
            }
            else{
                res.send(doc)
            }
        })
    }

    Controller.DeleteTransaction = function(req, res) {
        Transaction.findOneAndRemove({_id: req.params.id}, function (err, doc) {
            if(err){
                res.status(500).send(err);
            }
            else{
                res.send(true)
            }
        })

    }


    return Controller;
}