module.exports = function(app) {
    var Role = require('../models/Role');

    var Controller = {
        name: 'Role'
    }

    Controller.Roles = function(req, res) {

        var fields = req.query.fields? req.query.fields.join(' ') : '';
        Role.find({}, fields, function (err, docs) {
            if(err){
                res.status(500).send(err);
            }
            else {
                if(req.query.populate == '1'){
                    Role.populate(docs, { path: 'middleware', model: 'Middleware', select: 'name -_id' }, function(err, docs) {
                        if(err){
                            res.status(500).send(err)
                        }
                        else{
                            //Remove number field
                            docs.forEach(function(doc){
                                doc.number = undefined;
                            })
                            res.send(docs)
                        }
                    });
                }
                else{
                    //Remove number field
                    docs.forEach(function(doc){
                        doc.number = undefined;
                    })
                    res.send(docs);
                }

            }
        })
    }


    Controller.Role = function(req, res) {
        Role.findOne({_id: req.params.id}, function (err, doc) {
            if(err){
                res.status(500).send(err);
            }
            else if(doc){
                res.send(doc);
            }
            else{
                res.status(404).send(null)
            }
        })
    }


    Controller.AddRole = function(req, res) {
        Role.create(req.body, function (err, doc) {
            if(err){
                res.status(500).send(err);
            }
            else{
                res.send(doc)
            }
        })
    }


    Controller.UpdateRole = function(req, res) {
        Role.findOneAndUpdate({_id: req.params.id}, req.body, {new: true}, function (err, doc) {
            if(err){
                res.status(500).send(err);
            }
            else{
                res.send(doc)
            }
        })
    }

    Controller.DeleteRole = function(req, res) {
        Role.findOneAndRemove({_id: req.params.id}, function (err, doc) {
            if(err){
                res.status(500).send(err);
            }
            else{
                res.send(true)
            }
        })

    }


    return Controller;
}