module.exports = function(app) {
    var MessagingStyle = require('../models/MessagingStyle');

    var Controller = {
        name: 'MessagingStyle'
    }

    Controller.MessagingStyles = function(req, res) {

        var fields = req.query.fields? req.query.fields.join(' ') : '';
        MessagingStyle.find({}, fields, function (err, docs) {
            if(err){
                res.status(500).send(err);
            }
            else {
                if(req.query.populate == '1'){
                    MessagingStyle.populate(docs, { path: 'middleware', model: 'Middleware', select: 'name -_id' }, function(err, docs) {
                        if(err){
                            res.status(500).send(err)
                        }
                        else{
                            res.send(docs)
                        }
                    });
                }
                else{
                    res.send(docs);
                }

            }
        })
    }


    Controller.MessagingStyle = function(req, res) {
        MessagingStyle.findOne({_id: req.params.id}, function (err, doc) {
            if(err){
                res.status(500).send(err);
            }
            else if(doc){
                res.send(doc);
            }
            else{
                res.status(404).send(null)
            }
        })
    }


    Controller.AddMessagingStyle = function(req, res) {
        MessagingStyle.create(req.body, function (err, doc) {
            if(err){
                res.status(500).send(err);
            }
            else{
                res.send(doc)
            }
        })
    }


    Controller.UpdateMessagingStyle = function(req, res) {
        MessagingStyle.findOneAndUpdate({_id: req.params.id}, req.body, {new: true}, function (err, doc) {
            if(err){
                res.status(500).send(err);
            }
            else{
                res.send(doc)
            }
        })
    }

    Controller.DeleteMessagingStyle = function(req, res) {
        MessagingStyle.findOneAndRemove({_id: req.params.id}, function (err, doc) {
            if(err){
                res.status(500).send(err);
            }
            else{
                res.send(true)
            }
        })

    }


    return Controller;
}