module.exports = function(app) {
    var Benchmark = require('../models/Benchmark');
    var TransactionStyle = require('../models/TransactionStyle');

    var Controller = {
        name: 'Benchmark'
    }

    Controller.Benchmarks = function(req, res) {

        var fields = req.query.fields? req.query.fields.join(' ') : '';

        Benchmark.find({}, fields, function (err, docs) {
            if(err){
                res.status(500).send(err);
            }
            else {
                console.log("Query string")
                console.log(req.query)
                if(req.query.populate == '1'){
                    Benchmark.populate(docs, { path: 'middleware', model: 'Middleware', select: 'name -_id' }, function(err, docs) {
                        if(err){
                            res.status(500).send(err)
                        }
                        else{
                            res.send(docs)
                        }
                    });
                }
                else{
                    res.send(docs);
                }

            }
        })
    }


    Controller.Benchmark = function(req, res) {
        Benchmark.findOne({_id: req.params.id}, function (err, doc) {
            if(err){
                res.status(500).send(err);
            }
            else if(doc){
                res.send(doc);
            }
            else{
                res.status(404).send(null)
            }
        })
    }


    Controller.AddBenchmark = function(req, res) {
        Benchmark.create(req.body, function (err, doc) {
            if(err){
                res.status(500).send(err);
            }
            else{
                res.send(doc)
            }
        })
    }


    Controller.UpdateBenchmark = function(req, res) {
        Benchmark.findOneAndUpdate({_id: req.params.id}, req.body, {new: true}, function (err, doc) {
            if(err){
                res.status(500).send(err);
            }
            else{
                res.send(doc)
            }
        })
    }

    Controller.DeleteBenchmark = function(req, res) {
        Benchmark.findOneAndRemove({_id: req.params.id}, function (err, doc) {
            if(err){
                res.status(500).send(err);
            }
            else{
                res.send(true)
            }
        })

    }


    Controller.BenchmarkTransactionStyles = function(req, res) {
        var fields = req.query.fields? req.query.fields.join(' ') : '';

        TransactionStyle.find({benchmark: req.params.id}, fields, function (err, docs) {
            if(err){
                res.status(500).send(err);
            }
            else {
                TransactionStyle.populate(docs, { path: 'benchmark', model: 'Benchmark', select: 'name -_id' }, function(err, docs) {
                    if(err){
                        res.status(500).send(err)
                    }
                    else{
                        TransactionStyle.populate(docs, { path: 'middleware', model: 'Middleware', select: 'name -_id' }, function(err, docs) {
                            if(err){
                                res.status(500).send(err)
                            }
                            else{
                                res.send(docs)
                            }
                        });
                    }
                });
            }
        })

    }




    return Controller;
}