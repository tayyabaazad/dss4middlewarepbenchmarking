module.exports = function(app) {
    var ApplicationScenario = require('../models/ApplicationScenario');
    var Role = require('../models/Role');

    var Controller = {
        name: 'ApplicationScenario'
    }

    Controller.ApplicationScenarios = function(req, res) {
        var fields = req.query.fields? req.query.fields.join(' ') : '';

        ApplicationScenario.find({}, fields, function (err, docs) {
            if(err){
                res.status(500).send(err);
            }
            else {
                if(req.query.populate == '1'){
                    ApplicationScenario.populate(docs, { path: 'middleware', model: 'Middleware', select: 'name -_id' }, function(err, docs) {
                        if(err){
                            res.send(err)
                        }
                        else{
                            res.send(docs)
                        }
                    });
                }
                else{
                    res.send(docs);
                }

            }
        })
    }


    Controller.ApplicationScenario = function(req, res) {
        ApplicationScenario.findOne({_id: req.params.id}, function (err, doc) {
            if(err){
                res.status(500).send(err);
            }
            else if(doc){
                res.send(doc);
            }
            else{
                res.send(null)
            }
        })
    }


    Controller.AddApplicationScenario = function(req, res) {
        ApplicationScenario.create(req.body, function (err, doc) {
            if(err){
                res.status(500).send(err);
            }
            else{
                res.send(doc)
            }
        })
    }


    Controller.UpdateApplicationScenario = function(req, res) {
        ApplicationScenario.findOneAndUpdate({_id: req.params.id}, req.body, {new: true}, function (err, doc) {
            if(err){
                res.status(500).send(err);
            }
            else{
                res.send(doc)
            }
        })
    }

    Controller.DeleteApplicationScenario = function(req, res) {
        ApplicationScenario.findOneAndRemove({_id: req.params.id}, function (err, doc) {
            if(err){
                res.status(500).send(err);
            }
            else{
                res.send(true)
            }
        })

    }


    Controller.ApplicationScenarioRoles = function(req, res) {

        var query = req.params.asID == "all"? {}: {applicationScenario: req.params.asID}
        var fields = req.query.fields? req.query.fields.join(' ') : '';
        Role.find(query, fields, function (err, docs) {
            if(err){
                res.status(500).send(err);
            }
            else {
                Role.populate(docs, { path: 'applicationScenario', model: 'ApplicationScenario', select: 'model -_id' }, function(err, docs) {
                    if(err){
                        res.send(err)
                    }
                    else{
                        res.send(docs)
                    }
                });
            }
        })
    }

    return Controller;
}