module.exports = function(app) {
    var Tag = require('../models/Tag');

    var Controller = {
        name: 'Tag'
    }

    Controller.Tags = function(req, res) {
        Tag.find({}, function (err, docs) {
            if(err){
                res.send(err);
            }
            else {
                if(req.query.populate == '1'){
                    Tag.populate(docs, { path: 'middleware', model: 'Middleware', select: 'name -_id' }, function(err, docs) {
                        if(err){
                            res.send(err)
                        }
                        else{
                            res.send(docs)
                        }
                    });
                }
                else{
                    res.send(docs);
                }

            }
        })
    }


    Controller.AddTag = function(req, res) {
        Tag.create(req.body, function (err, tag) {
            if(err){
                res.send(err);
            }
            else {
                res.send(tag);
            }
        })
    }
    return Controller;
}