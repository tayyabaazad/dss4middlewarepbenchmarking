module.exports = function(app) {
    var Performance = require('../models/Performance');

    var Controller = {
        name: 'Performance'
    }

    Controller.Performances = function(req, res) {
        var fields = req.query.fields? req.query.fields.join(' ') : '';
        Performance.find({}, fields, function (err, docs) {
            if(err){
                res.status(500).send(err);
            }
            else {
                if(req.query.populate == '1'){
                    Performance.populate(docs, { path: 'middleware', model: 'Middleware', select: 'name -_id' }, function(err, docs) {
                        if(err){
                            res.status(500).send(err)
                        }
                        else{
                            res.send(docs)
                        }
                    });
                }
                else{
                    res.send(docs);
                }

            }
        })
    }


    Controller.Performance = function(req, res) {
        Performance.findOne({_id: req.params.id}, function (err, doc) {
            if(err){
                res.status(500).send(err);
            }
            else if(doc){
                res.send(doc);
            }
            else{
                res.status(404).send(null)
            }
        })
    }


    Controller.AddPerformance = function(req, res) {
        Performance.create(req.body, function (err, doc) {
            if(err){
                res.status(500).send(err);
            }
            else{
                res.send(doc)
            }
        })
    }


    Controller.UpdatePerformance = function(req, res) {
        Performance.findOneAndUpdate({_id: req.params.id}, req.body, {new: true}, function (err, doc) {
            if(err){
                res.status(500).send(err);
            }
            else{
                res.send(doc)
            }
        })
    }

    Controller.DeletePerformance = function(req, res) {
        Performance.findOneAndRemove({_id: req.params.id}, function (err, doc) {
            if(err){
                res.status(500).send(err);
            }
            else{
                res.send(true)
            }
        })

    }


    return Controller;
}