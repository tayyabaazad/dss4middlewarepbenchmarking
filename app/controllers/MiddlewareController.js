module.exports = function(app) {
    var Middleware = require('../models/Middleware');
    var ApplicationScenario = require('../models/ApplicationScenario');
    var Benchmark = require('../models/Benchmark');
    var Hardware = require('../models/Hardware');
    var Message = require('../models/Message');
    var MessageRatio = require('../models/MessageRatio');
    var MessagingStyle = require('../models/MessagingStyle');
    var Metric = require('../models/Metric');
    var Performance = require('../models/Performance');
    var Role = require('../models/MessageRatio');
    var Scalability = require('../models/Scalability');
    var Transaction = require('../models/Transaction');
    var TransactionStyle = require('../models/TransactionStyle');
    var Workload = require('../models/Workload');


    var Controller = {
        name: 'Middleware'
    }

    Controller.Middlewares = function(req, res) {
        var fields = req.query.fields? req.query.fields.join(' ') : '';

        Middleware.find({}, fields, function (err, docs) {
            if(err){
                res.status(500).send(err);
            }
            else {
                res.send(docs);
            }
        })
    }

    Controller.Middleware = function(req, res) {
        Middleware.find({_id: req.params.id}, function (err, docs) {
            if(err){
                res.status(500).send(err);
            }
            else {
                res.send(docs);
            }
        })
    }



    Controller.AddMiddleware = function(req, res) {
        Middleware.create(req.body, function (err, doc) {
            if(err){
                res.status(500).send(err);
            }
            else {
                res.send(doc);
            }
        })
    }

    Controller.UpdateMiddleware = function(req, res) {
        Middleware.findOneAndUpdate({_id: req.params.id}, req.body, {new: true}, function (err, doc) {
            if(err){
                res.status(500).send(err);
            }
            else {
                res.send(doc);
            }
        })
    }


    Controller.DeleteMiddleware = function(req, res) {
        Middleware.findOneAndRemove({_id: req.params.id}, function (err, doc) {
            if(err){
                res.status(500).send(err);
            }
            else {
                res.send(true);
            }
        })
    }

    Controller.MiddlewareApplicationScenarios = function(req, res) {
        var query =  {middleware: req.params.mwID};

        var fields = req.query.fields? req.query.fields.join(' ') : '';
        ApplicationScenario.find(query, fields)
            .exec(function (err, docs) {
                if(err){
                    res.status(500).send(err);
                }
                else {


                    ApplicationScenario.populate(docs, { path: 'middleware', model: 'Middleware', select: 'name -_id' }, function(err, docs) {
                        if(err){
                            res.status(500).send(err)
                        }
                        else{
                            res.send(docs)
                        }
                    });

                }
            })
    }

    Controller.MiddlewareBenchmarks = function(req, res) {
        var query = req.params.mwID == 'all'? {} : {middleware: req.params.mwID};
        var fields = req.query.fields? req.query.fields.join(' ') : '';
        Benchmark.find(query, fields, function (err, docs) {
            if(err){
                res.status(500).send(err);
            }
            else {
                Benchmark.populate(docs, { path: 'middleware', model: 'Middleware', select: 'name -_id' }, function(err, docs) {
                    if(err){
                        res.status(500).send(err)
                    }
                    else{
                        res.send(docs)
                    }
                });
            }
        })
    }
    Controller.MiddlewareHardwares = function(req, res) {
        var query = req.params.mwID == 'all'? {} : {middleware: req.params.mwID};
        var fields = req.query.fields? req.query.fields.join(' ') : '';
        Hardware.find(query, fields, function (err, docs) {
            if(err){
                res.status(500).send(err);
            }
            else {
                Hardware.populate(docs, { path: 'middleware', model: 'Middleware', select: 'name -_id' }, function(err, docs) {
                    if(err){
                        res.status(500).send(err)
                    }
                    else{
                        res.send(docs)
                    }
                });
            }
        })
    }
    Controller.MiddlewareMessages = function(req, res) {
        var query = req.params.mwID == 'all'? {} : {middleware: req.params.mwID};
        var fields = req.query.fields? req.query.fields.join(' ') : '';
        Message.find(query, fields, function (err, docs) {
            if(err){
                res.status(500).send(err);
            }
            else {
                Message.populate(docs, { path: 'middleware', model: 'Middleware', select: 'name -_id' }, function(err, docs) {
                    if(err){
                        res.status(500).send(err)
                    }
                    else{
                        res.send(docs)
                    }
                });
            }
        })
    }
    Controller.MiddlewareMessageRatios = function(req, res) {
        var query = req.params.mwID == 'all'? {} : {middleware: req.params.mwID};
        var fields = req.query.fields? req.query.fields.join(' ') : '';
        MessageRatio.find(query, fields, function (err, docs) {
            if(err){
                res.status(500).send(err);
            }
            else {
                MessageRatio.populate(docs, { path: 'middleware', model: 'Middleware', select: 'name -_id' }, function(err, docs) {
                    if(err){
                        res.status(500).send(err)
                    }
                    else{
                        res.send(docs)
                    }
                });
            }
        })
    }
    Controller.MiddlewareMessagingStyles = function(req, res) {
        var query = req.params.mwID == 'all'? {} : {middleware: req.params.mwID};
        var fields = req.query.fields? req.query.fields.join(' ') : '';
        MessagingStyle.find(query, fields, function (err, docs) {
            if(err){
                res.status(500).send(err);
            }
            else {
                MessagingStyle.populate(docs, { path: 'middleware', model: 'Middleware', select: 'name -_id' }, function(err, docs) {
                    if(err){
                        res.status(500).send(err)
                    }
                    else{
                        res.send(docs)
                    }
                });
            }
        })
    }
    Controller.MiddlewareMetrics = function(req, res) {
        var query = req.params.mwID == 'all'? {} : {middleware: req.params.mwID};
        var fields = req.query.fields? req.query.fields.join(' ') : '';
        Metric.find(query, fields, function (err, docs) {
            if(err){
                res.status(500).send(err);
            }
            else {
                Metric.populate(docs, { path: 'middleware', model: 'Middleware', select: 'name -_id' }, function(err, docs) {
                    if(err){
                        res.status(500).send(err)
                    }
                    else{
                        res.send(docs)
                    }
                });
            }
        })
    }
    Controller.MiddlewarePerformances = function(req, res) {
        var query = req.params.mwID == 'all'? {} : {middleware: req.params.mwID};
        var fields = req.query.fields? req.query.fields.join(' ') : '';
        Performance.find(query, fields, function (err, docs) {
            if(err){
                res.status(500).send(err);
            }
            else {
                Performance.populate(docs, { path: 'middleware', model: 'Middleware', select: 'name -_id' }, function(err, docs) {
                    if(err){
                        res.status(500).send(err)
                    }
                    else{
                        res.send(docs)
                    }
                });
            }
        })
    }
    Controller.MiddlewareRoles = function(req, res) {
        var query = req.params.mwID == 'all'? {} : {middleware: req.params.mwID};
        var fields = req.query.fields? req.query.fields.join(' ') : '';
        Role.find(query, fields, function (err, docs) {
            if(err){
                res.status(500).send(err);
            }
            else {
                Role.populate(docs, { path: 'middleware', model: 'Middleware', select: 'name -_id' }, function(err, docs) {
                    if(err){
                        res.status(500).send(err)
                    }
                    else{
                        //Remove number field
                        docs.forEach(function(doc){
                            doc.number = undefined;
                        })
                        res.send(docs)
                    }
                });
            }
        })
    }
    Controller.MiddlewareScalabilities = function(req, res) {
        var query = req.params.mwID == 'all'? {} : {middleware: req.params.mwID};
        var fields = req.query.fields? req.query.fields.join(' ') : '';
        Scalability.find(query, fields, function (err, docs) {
            if(err){
                res.status(500).send(err);
            }
            else {
                Scalability.populate(docs, { path: 'middleware', model: 'Middleware', select: 'name -_id' }, function(err, docs) {
                    if(err){
                        res.status(500).send(err)
                    }
                    else{
                        res.send(docs)
                    }
                });
            }
        })
    }
    Controller.MiddlewareTransactions = function(req, res) {
        var query = req.params.mwID == 'all'? {} : {middleware: req.params.mwID};
        var fields = req.query.fields? req.query.fields.join(' ') : '';
        Transaction.find(query, fields, function (err, docs) {
            if(err){
                res.status(500).send(err);
            }
            else {
                Transaction.populate(docs, { path: 'middleware', model: 'Middleware', select: 'name -_id' }, function(err, docs) {
                    if(err){
                        res.status(500).send(err)
                    }
                    else{
                        res.send(docs)
                    }
                });
            }
        })
    }
    Controller.MiddlewareTransactionStyles = function(req, res) {
        var query = req.params.mwID == 'all'? {} : {middleware: req.params.mwID};
        var fields = req.query.fields? req.query.fields.join(' ') : '';
        TransactionStyle.find(query, fields, function (err, docs) {
            if(err){
                res.status(500).send(err);
            }
            else {
                TransactionStyle.populate(docs, { path: 'middleware', model: 'Middleware', select: 'name -_id' }, function(err, docs) {
                    if(err){
                        res.status(500).send(err)
                    }
                    else{
                        res.send(docs)
                    }
                });
            }
        })
    }
    Controller.MiddlewareWorkloads = function(req, res) {
        var query = {middleware: req.params.mwID}; // where middleware is this
        var fields = req.query.fields? req.query.fields.join(' ') : '';
        Workload.find(query, fields, function (err, docs) {
            if(err){
                res.status(500).send(err);
            }
            else {
                // populate the middleware field i.e. populate the whole document in place of reference except for _id
                Workload.populate(docs, { path: 'middleware', model: 'Middleware', select: 'name -_id' }, function(err, docs) {
                    if(err){
                        res.status(500).send(err)
                    }
                    else{
                        res.send(docs)
                    }
                });
            }
        })
    }

    Controller.MiddlewareBenchmarkTransactionStyles = function(req, res) {

        var query = {}

        if(req.params.mwID != "all"){
            query.middleware = req.params.mwID
        }

        if(req.params.bmID != "all"){
            query.benchmark = req.params.bmID
        }


        console.log(query)
        var fields = req.query.fields? req.query.fields.join(' ') : '';
        TransactionStyle.find(query, fields, function (err, docs) {
            if(err){
                res.status(500).send(err);
            }
            else {
                res.send(docs);
            }
        })
    }

    return Controller;
}