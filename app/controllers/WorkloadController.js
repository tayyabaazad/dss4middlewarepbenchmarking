module.exports = function(app) {
    var Workload = require('../models/Workload');

    var Controller = {
        name: 'Workload'
    }

    Controller.Workloads = function(req, res) {
        var fields = req.query.fields? req.query.fields.join(' ') : '';
        Workload.find({}, fields, function (err, docs) {
            if(err){
                res.status(500).send(err);
            }
            else {
                if(req.query.populate == '1'){
                    Workload.populate(docs, { path: 'middleware', model: 'Middleware', select: 'name -_id' }, function(err, docs) {
                        if(err){
                            res.status(500).send(err)
                        }
                        else{
                            res.send(docs)
                        }
                    });
                }
                else{
                    res.send(docs);
                }

            }
        })
    }


    Controller.Workload = function(req, res) {
        Workload.findOne({_id: req.params.id}, function (err, doc) {
            if(err){
                res.status(500).send(err);
            }
            else if(doc){
                res.send(doc);
            }
            else{
                res.status(404).send(null)
            }
        })
    }


    Controller.AddWorkload = function(req, res) {
        Workload.create(req.body, function (err, doc) {
            if(err){
                res.status(500).send(err);
            }
            else{
                res.send(doc)
            }
        })
    }


    Controller.UpdateWorkload = function(req, res) {
        Workload.findOneAndUpdate({_id: req.params.id}, req.body, {new: true}, function (err, doc) {
            if(err){
                res.status(500).send(err);
            }
            else{
                res.send(doc)
            }
        })
    }

    Controller.DeleteWorkload = function(req, res) {
        Workload.findOneAndRemove({_id: req.params.id}, function (err, doc) {
            if(err){
                res.status(500).send(err);
            }
            else{
                res.send(true)
            }
        })

    }


    return Controller;
}