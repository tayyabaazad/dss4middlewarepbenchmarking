var mongoose = require('mongoose');  // grab the mongoose module

// module.exports allows us to pass this to other files when it is called
module.exports = mongoose.model('Benchmark', {  // define our benchmark model
    middleware: {type: mongoose.Schema.ObjectId, required: true, ref: 'Middleware'},
    consortium : {type : String, required: true},
    name : {type : String, required: true},
    type : {type : String}
});
