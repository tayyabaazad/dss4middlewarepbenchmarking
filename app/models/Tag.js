var mongoose = require('mongoose');

module.exports = mongoose.model('Tag', {
    name : {type : String, required: true},
    url: {type: String, required: true}

});
