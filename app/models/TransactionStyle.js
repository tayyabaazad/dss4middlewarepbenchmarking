var mongoose = require('mongoose');

module.exports = mongoose.model('TransactionStyle', {
    property : {type : String},
    benchmark: {type: mongoose.Schema.ObjectId, required: true, ref: 'Benchmark'},   // reference of benchmark document
    value : {type : String},
    middleware: {type: mongoose.Schema.ObjectId, required: true, ref: 'Middleware'}
});
