var mongoose = require('mongoose');

module.exports = mongoose.model('Metric', {
    middleware: {type: mongoose.Schema.ObjectId, required: true, ref: 'Middleware'},   // reference of middleware document
    type : {type : String},
    name : {type : String},
    additionalInfo : {type : String}
});
