var mongoose = require('mongoose');

module.exports = mongoose.model('ApplicationScenario', {
    middleware: {type: mongoose.Schema.ObjectId, required: true, ref: 'Middleware'},   // reference of middleware document
    model : {type : String, required: true},
    task : {type : String, required: true}
});
