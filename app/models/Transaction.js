var mongoose = require('mongoose');

module.exports = mongoose.model('Transaction', {
    middleware: {type: mongoose.Schema.ObjectId, required: true, ref: 'Middleware'},   // reference of middleware document
    dataType : [{type : String}],
    dimension : [{type : String}],
    type : {type : String, required: true},
    numberMin : {type : String, required: true},
    numberMax : {type : String, required: true},
    tableLayout : [{type : String}]
});
