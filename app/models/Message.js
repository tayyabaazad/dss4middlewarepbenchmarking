var mongoose = require('mongoose');

module.exports = mongoose.model('Message', {
    middleware: {type: mongoose.Schema.ObjectId, required: true, ref: 'Middleware'},   // reference of middleware document
    dataType : [{type : String}],
    dimension : [{type : String}],
    deliveryMode : [{type : String}],
    avgSizeMin : [{type : String}],
    avgSizeMax : [{type : String}],
    value : [{type : String}],
    destinationType : [{type : String}]
});
