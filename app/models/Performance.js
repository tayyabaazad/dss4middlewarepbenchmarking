var mongoose = require('mongoose');

module.exports = mongoose.model('Performance', {
    middleware: {type: mongoose.Schema.ObjectId, required: true, ref: 'Middleware'},   // reference of middleware document
    factors : [{type : String}]
});
