var mongoose = require('mongoose');

module.exports = mongoose.model('MessagingStyle', {
    dimension : {type : String},
    size : {type : String},
    deliveryMode : [{type : String}],
    middleware: {type: mongoose.Schema.ObjectId, required: true, ref: 'Middleware'}
});
