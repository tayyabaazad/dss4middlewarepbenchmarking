var mongoose = require('mongoose');

module.exports = mongoose.model('Role', {
    type : {type : String, required: true},
    number:  {type : Number, required: true},
    name : [{type : String}],
    applicationScenario: {type: mongoose.Schema.ObjectId, required: true, ref: 'ApplicationScenario'}   // reference of application scenario document
});
