var mongoose = require('mongoose');

module.exports = mongoose.model('Hardware', {
    middleware: {type: mongoose.Schema.ObjectId, required: true, ref: 'Middleware'},
    minRequirement : {type : String, required: true}
});
