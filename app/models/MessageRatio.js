var mongoose = require('mongoose');

module.exports = mongoose.model('MessageRatio', {
    messagingType : {type : String},
    numberMessaging : {type : String},
    numberBytes : {type : String},
    middleware: {type: mongoose.Schema.ObjectId, required: true, ref: 'Middleware'}
});
    