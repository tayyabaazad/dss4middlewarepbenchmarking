var mongoose = require('mongoose');

module.exports = mongoose.model('Workload', {
    middleware: {type: mongoose.Schema.ObjectId, required: true, ref: 'Middleware'},   // reference of middleware document
    type : [{type : String}],
    measure : [{type : String}],
    computation : {type : String}
});
