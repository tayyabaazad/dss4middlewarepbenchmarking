var mongoose = require('mongoose');

module.exports = mongoose.model('Scalability', {
    middleware: {type: mongoose.Schema.ObjectId, required: true, ref: 'Middleware'},
    name : {type: String, required: true},
    description : {type : String, required: true},
    trafficType : {type : String, required: true}
});
