var mongoose = require('mongoose');

module.exports = mongoose.model('Middleware', {
    name : {type : String, required: true}
});
