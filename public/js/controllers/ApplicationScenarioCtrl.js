
'use strict';
//view ctrl for application scenario
angular.module('DSS').controller('ApplicationScenarioController', function($scope, ApplicationScenarioService, AdminService, $location, MiddlewareService, $mdDialog, $timeout) {

    AdminService.signedInUser(function(data){

        },
        function(err){
            console.log(err)
            $location.path('/')
        });


    MiddlewareService.getAll(function(data){
            console.log(data)
            $scope.middlewares = data;
        },
        function(err){
            console.log(err)
        });


    ApplicationScenarioService.getAll(function(data){
            console.log(data)
            $scope.applicationScenarios = data;
        },
        function(err){
            console.log(err)
        });


    $scope.openCreateDialog = function(ev) {
        $mdDialog.show({
            controller: CreateApplicationScenarioController,
            templateUrl: '/views/applicationScenario/create.html',
            targetEvent: ev
        })
            .then(function(createdObj) {
                $scope.applicationScenarios.push(createdObj)
            }, function() {
            });
    };


    $scope.openUpdateDialog = function(applicationScenario, index, ev) {
        $mdDialog.show({
            controller: UpdateApplicationScenarioController,
            templateUrl: '/views/applicationScenario/update.html',
            locals: {
                applicationScenario : angular.copy(applicationScenario)
            },
            targetEvent: ev
        })
            .then(function(updatedObj) {
                if(updatedObj == true){
                    $scope.applicationScenarios.splice(index, 1)
                }
                else{
                    $scope.applicationScenarios[index] = updatedObj;
                }
            }, function() {
            });
    };

});

// create controller
function CreateApplicationScenarioController ($scope, ApplicationScenarioService, MiddlewareService, $mdDialog){

    $scope.applicationScenario = {};

    MiddlewareService.getAll(function(data){
            console.log(data)
            $scope.middlewares = data;
        },
        function(err){
            console.log(err)
        });

    $scope.create = function(form){

        if(!form.$valid){
            $scope.formInvalid = true;
            console.log(form.$error)
        }
        else{
            ApplicationScenarioService.create($scope.applicationScenario, function(data){
                console.log(data)
                $mdDialog.hide(data);
            }, function(){

            })
        }

    }

    $scope.close = function () {
        $mdDialog.cancel();
    }
}

function UpdateApplicationScenarioController ($scope, applicationScenario, ApplicationScenarioService, MiddlewareService, $mdDialog){

    $scope.applicationScenario = applicationScenario;

    MiddlewareService.getAll(function(data){
            console.log(data)
            $scope.middlewares = data;
        },
        function(err){
            console.log(err)
        });

    $scope.update = function(form){

        if(!form.$valid){
            $scope.formInvalid = true;
            console.log(form.$error)
        }
        else{
            ApplicationScenarioService.update($scope.applicationScenario, function(data){
                $mdDialog.hide(data);
            }, function(){

            })
        }

    }


    $scope.delete = function(){

        ApplicationScenarioService.delete($scope.applicationScenario._id, function(data){
            $mdDialog.hide(true);
        }, function(){

        })
    };

    $scope.close = function () {
        $mdDialog.cancel();
    }
}
