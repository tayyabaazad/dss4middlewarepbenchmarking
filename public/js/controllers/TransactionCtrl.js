
'use strict';

angular.module('DSS').controller('TransactionController', function($scope, AdminService, $location,TransactionService, $mdDialog, $timeout) {

    AdminService.signedInUser(function(data){

        },
        function(err){
            console.log(err)
            $location.path('/')
        });
    TransactionService.getAll(function(data){
            $scope.transactions = data;
        },
        function(err){
            console.log(err)
        });


    $scope.openCreateDialog = function(ev) {
        $mdDialog.show({
            controller: CreateTransactionController,
            templateUrl: '/views/transaction/create.html',
            targetEvent: ev
        })
            .then(function(createdObj) {
                $scope.transactions.push(createdObj)
            }, function() {
            });
    };


    $scope.openUpdateDialog = function(transaction, index, ev) {
        $mdDialog.show({
            controller: UpdateTransactionController,
            templateUrl: '/views/transaction/update.html',
            locals: {
                transaction : angular.copy(transaction)
            },
            targetEvent: ev
        })
            .then(function(updatedObj) {
                if(updatedObj == true){
                    $scope.transactions.splice(index, 1)
                }
                else{
                    $scope.transactions[index] = updatedObj;
                }
            }, function() {
            });
    };

});


function CreateTransactionController ($scope, TransactionService, MiddlewareService, $mdDialog){

    $scope.transaction = {};

    MiddlewareService.getAll(function(data){
            $scope.middlewares = data;
        },
        function(err){
            console.log(err)
        });

    $scope.create = function(form){

        if(!form.$valid){
            $scope.formInvalid = true
        }
        else{
            TransactionService.create($scope.transaction, function(data){
                $mdDialog.hide(data);
            }, function(){

            })
        }


    }

    $scope.close = function () {
        $mdDialog.cancel();
    }
}


function UpdateTransactionController ($scope, transaction, TransactionService, MiddlewareService, $mdDialog){

    $scope.transaction = transaction;

    MiddlewareService.getAll(function(data){
            $scope.middlewares = data;
        },
        function(err){
            console.log(err)
        });

    $scope.update = function(form){

        if(form.$invalid){
            $scope.formInvalid = true;
        }
        else{
            TransactionService.update($scope.transaction, function(data){
                $mdDialog.hide(data);
            }, function(){

            })
        }

    }


    $scope.delete = function(){

        TransactionService.delete($scope.transaction._id, function(data){
            $mdDialog.hide(true);
        }, function(){

        })
    };

    $scope.close = function () {
        $mdDialog.cancel();
    }
}
