
'use strict';

angular.module('DSS').controller('TransactionStyleController', function($scope, AdminService, $location, TransactionStyleService, $mdDialog, $timeout) {
    AdminService.signedInUser(function(data){

        },
        function(err){
            console.log(err)
            $location.path('/')
        });

    TransactionStyleService.getAll(function(data){
            $scope.transactionStyles = data;
        },
        function(err){
            console.log(err)
        });


    $scope.openCreateDialog = function(ev) {
        $mdDialog.show({
            controller: CreateTransactionStyleController,
            templateUrl: '/views/transactionStyle/create.html',
            targetEvent: ev
        })
            .then(function(createdObj) {
                $scope.transactionStyles.push(createdObj)
            }, function() {
            });
    };


    $scope.openUpdateDialog = function(transactionStyle, index, ev) {
        $mdDialog.show({
            controller: UpdateTransactionStyleController,
            templateUrl: '/views/transactionStyle/update.html',
            locals: {
                transactionStyle : angular.copy(transactionStyle)
            },
            targetEvent: ev
        })
            .then(function(updatedObj) {
                if(updatedObj == true){
                    $scope.transactionStyles.splice(index, 1)
                }
                else{
                    $scope.transactionStyles[index] = updatedObj;
                }
            }, function() {
            });
    };

});


function CreateTransactionStyleController ($scope, TransactionStyleService, MiddlewareService, BenchmarkService, $mdDialog){

    $scope.transactionStyle = {};

    MiddlewareService.getAll(function(data){
            $scope.middlewares = data;
        },
        function(err){
            console.log(err)
        });

    BenchmarkService.getAll(function(data){
            $scope.benchmarks = data;
        },
        function(err){
            console.log(err)
        });


    $scope.create = function(form){

        if(!form.$valid){
            $scope.formInvalid = true
        }
        else{
            TransactionStyleService.create($scope.transactionStyle, function(data){
                $mdDialog.hide(data);
            }, function(){

            })
        }


    }

    $scope.close = function () {
        $mdDialog.cancel();
    }
}


function UpdateTransactionStyleController ($scope, transactionStyle, TransactionStyleService, MiddlewareService, BenchmarkService, $mdDialog){

    $scope.transactionStyle = transactionStyle;

    MiddlewareService.getAll(function(data){
            $scope.middlewares = data;
        },
        function(err){
            console.log(err)
        });

    BenchmarkService.getAll(function(data){
            $scope.benchmarks = data;
        },
        function(err){
            console.log(err)
        });

    $scope.update = function(form){

        if(form.$invalid){
            $scope.formInvalid = true;
        }
        else{
            TransactionStyleService.update($scope.transactionStyle, function(data){
                $mdDialog.hide(data);
            }, function(){

            })
        }

    }


    $scope.delete = function(){

        TransactionStyleService.delete($scope.transactionStyle._id, function(data){
            $mdDialog.hide(true);
        }, function(){

        })
    };

    $scope.close = function () {
        $mdDialog.cancel();
    }
}
