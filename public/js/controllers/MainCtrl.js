'use strict';
angular.module('DSS').controller('MainController', function($scope, $mdSidenav, MiddlewareService, AdminService, $location, $rootScope, $mdUtil, $animate) {

    MiddlewareService.getAll(function(data){
            console.log(data)
            $rootScope.middlewares = data;
        },
        function(err){
            console.log(err)
        });


    AdminService.signedInUser(function(data){
            $rootScope.admin = data;
        },
        function(err){
            console.log(err)
        });

    $scope.signOut = function (){

        AdminService.signOut(function(data){
                $location.path('/');
                $rootScope.admin = undefined;
            },
            function(err){
                console.log(err)
            });
    }

});
