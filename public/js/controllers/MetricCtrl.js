
'use strict';

angular.module('DSS').controller('MetricController', function($scope,AdminService, $location, MetricService, $mdDialog, $timeout) {
    AdminService.signedInUser(function(data){

        },
        function(err){
            console.log(err)
            $location.path('/')
        });

    MetricService.getAll(function(data){
            $scope.metrics = data;
        },
        function(err){
            console.log(err)
        });


    $scope.openCreateDialog = function(ev) {
        $mdDialog.show({
            controller: CreateMetricController,
            templateUrl: '/views/metric/create.html',
            targetEvent: ev
        })
            .then(function(createdObj) {
                $scope.metrics.push(createdObj)
            }, function() {
            });
    };


    $scope.openUpdateDialog = function(metric, index, ev) {
        $mdDialog.show({
            controller: UpdateMetricController,
            templateUrl: '/views/metric/update.html',
            locals: {
                metric : angular.copy(metric)
            },
            targetEvent: ev
        })
            .then(function(updatedObj) {
                if(updatedObj == true){
                    $scope.metrics.splice(index, 1)
                }
                else{
                    $scope.metrics[index] = updatedObj;
                }
            }, function() {
            });
    };

});


function CreateMetricController ($scope, MetricService, MiddlewareService, $mdDialog){

    $scope.metric = {};

    MiddlewareService.getAll(function(data){
            $scope.middlewares = data;
        },
        function(err){
            console.log(err)
        });

    $scope.create = function(form){

        console.log($scope.metric)
        if(!form.$valid){
            $scope.formInvalid = true
        }
        else{
            MetricService.create($scope.metric, function(data){
                console.log(data)
                $mdDialog.hide(data);
            }, function(){

            })
        }


    }

    $scope.close = function () {
        $mdDialog.cancel();
    }
}


function UpdateMetricController ($scope, metric, MetricService, MiddlewareService, $mdDialog){

    $scope.metric = metric;

    console.log($scope.metric)
    MiddlewareService.getAll(function(data){
            $scope.middlewares = data;
        },
        function(err){
            console.log(err)
        });

    $scope.update = function(form){

        if(form.$invalid){
            $scope.formInvalid = true;
        }
        else{
            MetricService.update($scope.metric, function(data){
                $mdDialog.hide(data);
            }, function(){

            })
        }

    }


    $scope.delete = function(){

        MetricService.delete($scope.metric._id, function(data){
            $mdDialog.hide(true);
        }, function(){

        })
    };

    $scope.close = function () {
        $mdDialog.cancel();
    }
}
