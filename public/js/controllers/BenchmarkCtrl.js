
'use strict';

angular.module('DSS').controller('BenchmarkController', function($scope,AdminService, $location, BenchmarkService, $mdDialog, $timeout) {

    AdminService.signedInUser(function(data){

        },
        function(err){
            console.log(err)
            $location.path('/')
        });

    BenchmarkService.getAll(function(data){
            console.log(data)
            $scope.benchmarks = data;
        },
        function(err){
            console.log(err)
        });


    $scope.openCreateDialog = function(ev) {
        $mdDialog.show({
            controller: CreateBenchmarkController,
            templateUrl: '/views/benchmark/create.html',
            targetEvent: ev
        })
            .then(function(createdObj) {
                $scope.benchmarks.push(createdObj)
            }, function() {
            });
    };


    $scope.openUpdateDialog = function(benchmark, index, ev) {
        $mdDialog.show({
            controller: UpdateBenchmarkController,
            templateUrl: '/views/benchmark/update.html',
            locals: {
                benchmark : angular.copy(benchmark)
            },
            targetEvent: ev
        })
            .then(function(updatedObj) {
                if(updatedObj == true){
                    $scope.benchmarks.splice(index, 1)
                }
                else{
                    $scope.benchmarks[index] = updatedObj;
                }
            }, function() {
            });
    };

});


function CreateBenchmarkController ($scope, BenchmarkService, MiddlewareService, $mdDialog){

    $scope.benchmark = {};

    MiddlewareService.getAll(function(data){
            console.log(data)
            $scope.middlewares = data;
        },
        function(err){
            console.log(err)
        });

    $scope.create = function(form){
        if(!form.$valid){
            $scope.formInvaid = true;
        }
        else{
            BenchmarkService.create($scope.benchmark, function(data){
                $mdDialog.hide(data);
            }, function(){

            })
        }

    }

    $scope.close = function () {
        $mdDialog.cancel();
    }
}


function UpdateBenchmarkController ($scope, benchmark, BenchmarkService, MiddlewareService, $mdDialog){

    $scope.benchmark = benchmark;

    MiddlewareService.getAll(function(data){
            $scope.middlewares = data;
        },
        function(err){
            console.log(err)
        });

    $scope.update = function(form){
        console.log("update")
        if(!form.$valid){
            $scope.formInvaid = true;
        }
        else{
            BenchmarkService.update($scope.benchmark, function(data){
                $mdDialog.hide(data);
            }, function(){

            })
        }

    }


    $scope.delete = function(){

        BenchmarkService.delete($scope.benchmark._id, function(data){
            $mdDialog.hide(true);
        }, function(){

        })
    };

    $scope.close = function () {
        $mdDialog.cancel();
    }
}
