
'use strict';

angular.module('DSS').controller('MessageController', function($scope, AdminService, $location,MessageService, $mdDialog, $timeout) {

    AdminService.signedInUser(function(data){

        },
        function(err){
            console.log(err)
            $location.path('/')
        });
    MessageService.getAll(function(data){
            console.log(data)
            $scope.messages = data;
        },
        function(err){
            console.log(err)
        });


    $scope.openCreateDialog = function(ev) {
        $mdDialog.show({
            controller: CreateMessageController,
            templateUrl: '/views/message/create.html',
            targetEvent: ev
        })
            .then(function(createdObj) {
                $scope.messages.push(createdObj)
            }, function() {
            });
    };


    $scope.openUpdateDialog = function(message, index, ev) {
        $mdDialog.show({
            controller: UpdateMessageController,
            templateUrl: '/views/message/update.html',
            locals: {
                message : angular.copy(message)
            },
            targetEvent: ev
        })
            .then(function(updatedObj) {
                if(updatedObj == true){
                    $scope.messages.splice(index, 1)
                }
                else{
                    $scope.messages[index] = updatedObj;
                }
            }, function() {
            });
    };

});


function CreateMessageController ($scope, MessageService, MiddlewareService, $mdDialog){

    $scope.message = {};

    MiddlewareService.getAll(function(data){
            $scope.middlewares = data;
        },
        function(err){
            console.log(err)
        });

    $scope.create = function(form){

        if(!form.$valid){
            $scope.formInvalid = true
        }
        else{
            MessageService.create($scope.message, function(data){
                $mdDialog.hide(data);
            }, function(){

            })
        }


    }

    $scope.close = function () {
        $mdDialog.cancel();
    }
}


function UpdateMessageController ($scope, message, MessageService, MiddlewareService, $mdDialog){

    $scope.message = message;

    MiddlewareService.getAll(function(data){
            $scope.middlewares = data;
        },
        function(err){
            console.log(err)
        });

    $scope.update = function(form){

        if(form.$invalid){
            $scope.formInvalid = true;
        }
        else{
            MessageService.update($scope.message, function(data){
                $mdDialog.hide(data);
            }, function(){

            })
        }

    }


    $scope.delete = function(){

        MessageService.delete($scope.message._id, function(data){
            $mdDialog.hide(true);
        }, function(){

        })
    };

    $scope.close = function () {
        $mdDialog.cancel();
    }
}
