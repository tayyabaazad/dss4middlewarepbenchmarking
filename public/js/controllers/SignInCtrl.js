'use strict';
angular.module('DSS').controller('SignInController', function($scope, $mdSidenav, AdminService, $timeout, $location, $rootScope, $mdUtil, $animate) {

   $scope.signIn = function(){
       AdminService.signIn($scope.user, function(data){
               $rootScope.admin = data;
               $location.path('/');
       },
       function(err){

           console.log(err)

           $scope.errorMessage = err;
           $timeout(function(){
               $scope.errorMessage = null;
           }, 5000)
       })
   }


});
