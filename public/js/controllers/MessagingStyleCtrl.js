
'use strict';

angular.module('DSS').controller('MessagingStyleController', function($scope,AdminService, $location, MessagingStyleService, $mdDialog, $timeout) {

    AdminService.signedInUser(function(data){

        },
        function(err){
            console.log(err)
            $location.path('/')
        });
    MessagingStyleService.getAll(function(data){
            $scope.messagingStyles = data;
        },
        function(err){
            console.log(err)
        });


    $scope.openCreateDialog = function(ev) {
        $mdDialog.show({
            controller: CreateMessagingStyleController,
            templateUrl: '/views/messagingStyle/create.html',
            targetEvent: ev
        })
            .then(function(createdObj) {
                $scope.messagingStyles.push(createdObj)
            }, function() {
            });
    };


    $scope.openUpdateDialog = function(messagingStyle, index, ev) {
        $mdDialog.show({
            controller: UpdateMessagingStyleController,
            templateUrl: '/views/messagingStyle/update.html',
            locals: {
                messagingStyle : angular.copy(messagingStyle)
            },
            targetEvent: ev
        })
            .then(function(updatedObj) {
                if(updatedObj == true){
                    $scope.messagingStyles.splice(index, 1)
                }
                else{
                    $scope.messagingStyles[index] = updatedObj;
                }
            }, function() {
            });
    };

});


function CreateMessagingStyleController ($scope, MessagingStyleService, MiddlewareService, $mdDialog){

    $scope.messagingStyle = {};

    MiddlewareService.getAll(function(data){
            $scope.middlewares = data;
        },
        function(err){
            console.log(err)
        });

    $scope.create = function(form){

        if(!form.$valid){
            $scope.formInvalid = true
        }
        else{
            MessagingStyleService.create($scope.messagingStyle, function(data){
                $mdDialog.hide(data);
            }, function(){

            })
        }


    }

    $scope.close = function () {
        $mdDialog.cancel();
    }
}


function UpdateMessagingStyleController ($scope, messagingStyle, MessagingStyleService, MiddlewareService, $mdDialog){

    $scope.messagingStyle = messagingStyle;

    MiddlewareService.getAll(function(data){
            $scope.middlewares = data;
        },
        function(err){
            console.log(err)
        });

    $scope.update = function(form){

        if(form.$invalid){
            $scope.formInvalid = true;
        }
        else{
            MessagingStyleService.update($scope.messagingStyle, function(data){
                $mdDialog.hide(data);
            }, function(){

            })
        }

    }


    $scope.delete = function(){

        MessagingStyleService.delete($scope.messagingStyle._id, function(data){
            $mdDialog.hide(true);
        }, function(){

        })
    };

    $scope.close = function () {
        $mdDialog.cancel();
    }
}
