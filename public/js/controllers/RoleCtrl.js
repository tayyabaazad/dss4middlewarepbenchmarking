
'use strict';

angular.module('DSS').controller('RoleController', function($scope, RoleService,AdminService, $location, $mdDialog, $timeout) {

    AdminService.signedInUser(function(data){

        },
        function(err){
            console.log(err)
            $location.path('/')
        });
    RoleService.getAll(function(data){
            $scope.roles = data;
        },
        function(err){
            console.log(err)
        });


    $scope.openCreateDialog = function(ev) {
        $mdDialog.show({
            controller: CreateRoleController,
            templateUrl: '/views/role/create.html',
            targetEvent: ev
        })
            .then(function(createdObj) {
                $scope.roles.push(createdObj)
            }, function() {
            });
    };


    $scope.openUpdateDialog = function(role, index, ev) {
        $mdDialog.show({
            controller: UpdateRoleController,
            templateUrl: '/views/role/update.html',
            locals: {
                role : angular.copy(role)
            },
            targetEvent: ev
        })
            .then(function(updatedObj) {
                if(updatedObj == true){
                    $scope.roles.splice(index, 1)
                }
                else{
                    $scope.roles[index] = updatedObj;
                }
            }, function() {
            });
    };

});


function CreateRoleController ($scope, RoleService, ApplicationScenarioService, $mdDialog){

    $scope.role = {};

    ApplicationScenarioService.getAll(function(data){
            $scope.applicationScenarios = data;
        },
        function(err){
            console.log(err)
        });

    $scope.create = function(form){

        if(!form.$valid){
            $scope.formInvalid = true
        }
        else{

            RoleService.create($scope.role, function(data){
                console.log(data)
                $mdDialog.hide(data);
            }, function(){

            })
        }

    }

    $scope.close = function () {
        $mdDialog.cancel();
    }
}


function UpdateRoleController ($scope, role, RoleService, ApplicationScenarioService, $mdDialog){

    $scope.role = role;

    ApplicationScenarioService.getAll(function(data){
            $scope.applicationScenarios = data;
        },
        function(err){
            console.log(err)
        });

    $scope.update = function(form){

        if(form.$invalid){
            $scope.formInvalid = true;
        }
        else{
            RoleService.update($scope.role, function(data){
                $mdDialog.hide(data);
            }, function(){

            })
        }

    }


    $scope.delete = function(){

        RoleService.delete($scope.role._id, function(data){
            $mdDialog.hide(true);
        }, function(){

        })
    };

    $scope.close = function () {
        $mdDialog.cancel();
    }
}
