
'use strict';

angular.module('DSS').controller('MiddlewareController', function($scope,AdminService, $location, MiddlewareService, $mdDialog, $timeout) {

    AdminService.signedInUser(function(data){

        },
        function(err){
            console.log(err)
            $location.path('/')
        });
    MiddlewareService.getAll(function(data){
            $scope.middlewares = data;
        },
        function(err){
            console.log(err)
        });


    $scope.isBasicMiddleware = function (middleware){

        return middleware.name == 'DBMS' || middleware.name == 'Message Oriented Middleware (MOM)' || middleware.name == 'Java Server';
    }

    $scope.openCreateDialog = function(ev) {
        $mdDialog.show({
            controller: CreateMiddlewareController,
            templateUrl: '/views/middleware/create.html',
            targetEvent: ev
        })
            .then(function(createdObj) {
                $scope.middlewares.push(createdObj)
            }, function() {
            });
    };


    $scope.openUpdateDialog = function(middleware, index, ev) {
        $mdDialog.show({
            controller: UpdateMiddlewareController,
            templateUrl: '/views/middleware/update.html',
            locals: {
                middleware : angular.copy(middleware)
            },
            targetEvent: ev
        })
            .then(function(updatedObj) {
                if(updatedObj == true){
                    $scope.middlewares.splice(index, 1)
                }
                else{
                    $scope.middlewares[index] = updatedObj;
                }
            }, function() {
            });
    };

});


function CreateMiddlewareController ($scope, MiddlewareService, $mdDialog){

    $scope.middleware = {};

    $scope.create = function(form){

        if(!form.$valid){
            $scope.formInvalid = true
        }
        else{
            MiddlewareService.create($scope.middleware, function(data){
                $mdDialog.hide(data);
            }, function(){

            })
        }


    }

    $scope.close = function () {
        $mdDialog.cancel();
    }
}


function UpdateMiddlewareController ($scope, middleware, MiddlewareService, $mdDialog){

    $scope.middleware = middleware;

    $scope.update = function(form){

        if(form.$invalid){
            $scope.formInvalid = true;
        }
        else{
            MiddlewareService.update($scope.middleware, function(data){
                $mdDialog.hide(data);
            }, function(){

            })
        }

    }


    $scope.delete = function(){

        MiddlewareService.delete($scope.middleware._id, function(data){
            $mdDialog.hide(true);
        }, function(){

        })
    };

    $scope.close = function () {
        $mdDialog.cancel();
    }
}
