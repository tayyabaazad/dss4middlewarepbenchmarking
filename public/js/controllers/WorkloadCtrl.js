
'use strict';

angular.module('DSS').controller('WorkloadController', function($scope, AdminService, $location, WorkloadService, $mdDialog, $timeout) {
    AdminService.signedInUser(function(data){

        },
        function(err){
            console.log(err)
            $location.path('/')
        });

    WorkloadService.getAll(function(data){
            $scope.workloads = data;
        },
        function(err){
            console.log(err)
        });


    $scope.openCreateDialog = function(ev) {
        $mdDialog.show({
            controller: CreateWorkloadController,
            templateUrl: '/views/workload/create.html',
            targetEvent: ev
        })
            .then(function(createdObj) {
                $scope.workloads.push(createdObj)
            }, function() {
            });
    };


    $scope.openUpdateDialog = function(workload, index, ev) {
        $mdDialog.show({
            controller: UpdateWorkloadController,
            templateUrl: '/views/workload/update.html',
            locals: {
                workload : angular.copy(workload)
            },
            targetEvent: ev
        })
            .then(function(updatedObj) {
                if(updatedObj == true){
                    $scope.workloads.splice(index, 1)
                }
                else{
                    $scope.workloads[index] = updatedObj;
                }
            }, function() {
            });
    };

});


function CreateWorkloadController ($scope, WorkloadService, MiddlewareService, $mdDialog){

    $scope.workload = {};

    MiddlewareService.getAll(function(data){
            $scope.middlewares = data;
        },
        function(err){
            console.log(err)
        });

    $scope.create = function(form){

        if(!form.$valid){
            $scope.formInvalid = true
        }
        else{

            WorkloadService.create($scope.workload, function(data){
                $mdDialog.hide(data);
            }, function(){

            })
        }

    }

    $scope.close = function () {
        $mdDialog.cancel();
    }
}


function UpdateWorkloadController ($scope, workload, WorkloadService, MiddlewareService, $mdDialog){

    $scope.workload = workload;

    MiddlewareService.getAll(function(data){
            $scope.middlewares = data;
        },
        function(err){
            console.log(err)
        });


    $scope.update = function(form){

        if(form.$invalid){
            $scope.formInvalid = true;
        }
        else{
            WorkloadService.update($scope.workload, function(data){
                $mdDialog.hide(data);
            }, function(){

            })
        }

    }


    $scope.delete = function(){

        WorkloadService.delete($scope.workload._id, function(data){
            $mdDialog.hide(true);
        }, function(){

        })
    };

    $scope.close = function () {
        $mdDialog.cancel();
    }
}
