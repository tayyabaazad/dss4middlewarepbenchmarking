
'use strict';

angular.module('DSS').controller('PerformanceController', function($scope, AdminService, $location,PerformanceService, $mdDialog, $timeout) {

    AdminService.signedInUser(function(data){

        },
        function(err){
            console.log(err)
            $location.path('/')
        });
    PerformanceService.getAll(function(data){
            console.log(data)
            $scope.performances = data;
        },
        function(err){
            console.log(err)
        });


    $scope.openCreateDialog = function(ev) {
        $mdDialog.show({
            controller: CreatePerformanceController,
            templateUrl: '/views/performance/create.html',
            targetEvent: ev
        })
            .then(function(createdObj) {
                $scope.performances.push(createdObj)
            }, function() {
            });
    };


    $scope.openUpdateDialog = function(performance, index, ev) {
        $mdDialog.show({
            controller: UpdatePerformanceController,
            templateUrl: '/views/performance/update.html',
            locals: {
                performance : angular.copy(performance)
            },
            targetEvent: ev
        })
            .then(function(updatedObj) {
                if(updatedObj == true){
                    $scope.performances.splice(index, 1)
                }
                else{
                    $scope.performances[index] = updatedObj;
                }
            }, function() {
            });
    };

});


function CreatePerformanceController ($scope, PerformanceService, MiddlewareService, $mdDialog){

    $scope.performance = {};

    MiddlewareService.getAll(function(data){
            $scope.middlewares = data;
        },
        function(err){
            console.log(err)
        });

    $scope.create = function(form){

        if(!form.$valid){
            $scope.formInvalid = true
        }
        else{
            PerformanceService.create($scope.performance, function(data){
                $mdDialog.hide(data);
            }, function(){

            })
        }


    }

    $scope.close = function () {
        $mdDialog.cancel();
    }
}


function UpdatePerformanceController ($scope, performance, PerformanceService,MiddlewareService, $mdDialog){

    $scope.performance = performance;

    MiddlewareService.getAll(function(data){
            $scope.middlewares = data;
        },
        function(err){
            console.log(err)
        });

    $scope.update = function(form){

        if(form.$invalid){
            $scope.formInvalid = true;
        }
        else{
            PerformanceService.update($scope.performance, function(data){
                $mdDialog.hide(data);
            }, function(){

            })
        }

    }


    $scope.delete = function(){

        PerformanceService.delete($scope.performance._id, function(data){
            $mdDialog.hide(true);
        }, function(){

        })
    };

    $scope.close = function () {
        $mdDialog.cancel();
    }
}
