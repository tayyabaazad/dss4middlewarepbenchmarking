
'use strict';

angular.module('DSS').controller('MessageRatioController', function($scope, AdminService, $location,MessageRatioService, $mdDialog, $timeout) {
    AdminService.signedInUser(function(data){

        },
        function(err){
            console.log(err)
            $location.path('/')
        });

    MessageRatioService.getAll(function(data){
            $scope.messageRatios = data;
        },
        function(err){
            console.log(err)
        });


    $scope.openCreateDialog = function(ev) {
        $mdDialog.show({
            controller: CreateMessageRatioController,
            templateUrl: '/views/messageRatio/create.html',
            targetEvent: ev
        })
            .then(function(createdObj) {
                $scope.messageRatios.push(createdObj)
            }, function() {
            });
    };


    $scope.openUpdateDialog = function(messageRatio, index, ev) {
        $mdDialog.show({
            controller: UpdateMessageRatioController,
            templateUrl: '/views/messageRatio/update.html',
            locals: {
                messageRatio : angular.copy(messageRatio)
            },
            targetEvent: ev
        })
            .then(function(updatedObj) {
                if(updatedObj == true){
                    $scope.messageRatios.splice(index, 1)
                }
                else{
                    $scope.messageRatios[index] = updatedObj;
                }
            }, function() {
            });
    };

});


function CreateMessageRatioController ($scope, MessageRatioService, MiddlewareService, $mdDialog){

    $scope.messageRatio = {};

    MiddlewareService.getAll(function(data){
            $scope.middlewares = data;
        },
        function(err){
            console.log(err)
        });

    $scope.create = function(form){

        if(!form.$valid){
            $scope.formInvalid = true
        }
        else{
            MessageRatioService.create($scope.messageRatio, function(data){
                console.log(data)
                $mdDialog.hide(data);
            }, function(){

            })
        }


    }

    $scope.close = function () {
        $mdDialog.cancel();
    }
}


function UpdateMessageRatioController ($scope, messageRatio, MessageRatioService, MiddlewareService, $mdDialog){

    $scope.messageRatio = messageRatio;

    MiddlewareService.getAll(function(data){
            $scope.middlewares = data;
        },
        function(err){
            console.log(err)
        });

    $scope.update = function(form){

        if(form.$invalid){
            $scope.formInvalid = true;
        }
        else{
            MessageRatioService.update($scope.messageRatio, function(data){
                $mdDialog.hide(data);
            }, function(){

            })
        }

    }


    $scope.delete = function(){

        MessageRatioService.delete($scope.messageRatio._id, function(data){
            $mdDialog.hide(true);
        }, function(){

        })
    };

    $scope.close = function () {
        $mdDialog.cancel();
    }
}
