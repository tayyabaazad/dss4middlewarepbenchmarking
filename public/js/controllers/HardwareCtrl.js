
'use strict';

angular.module('DSS').controller('HardwareController', function($scope, AdminService, $location,HardwareService, $mdDialog, $timeout) {
    AdminService.signedInUser(function(data){

        },
        function(err){
            console.log(err)
            $location.path('/')
        });

    HardwareService.getAll(function(data){
            $scope.hardwares = data;
        },
        function(err){
            console.log(err)
        });


    $scope.openCreateDialog = function(ev) {
        $mdDialog.show({
            controller: CreateHardwareController,
            templateUrl: '/views/hardware/create.html',
            targetEvent: ev
        })
            .then(function(createdObj) {
                $scope.hardwares.push(createdObj)
            }, function() {
            });
    };


    $scope.openUpdateDialog = function(hardware, index, ev) {
        $mdDialog.show({
            controller: UpdateHardwareController,
            templateUrl: '/views/hardware/update.html',
            locals: {
                hardware : angular.copy(hardware)
            },
            targetEvent: ev
        })
            .then(function(updatedObj) {
                if(updatedObj == true){
                    $scope.hardwares.splice(index, 1)
                }
                else{
                    $scope.hardwares[index] = updatedObj;
                }
            }, function() {
            });
    };

});


function CreateHardwareController ($scope, HardwareService, MiddlewareService, $mdDialog){

    $scope.hardware = {};

    MiddlewareService.getAll(function(data){
            $scope.middlewares = data;
        },
        function(err){
            console.log(err)
        });


    $scope.create = function(form){

        if(!form.$valid){
            $scope.formInvalid = true
        }
        else{

            HardwareService.create($scope.hardware, function(data){
                console.log(data)
                $mdDialog.hide(data);
            }, function(){

            })
        }
    }

    $scope.close = function () {
        $mdDialog.cancel();
    }
}


function UpdateHardwareController ($scope, hardware, HardwareService, MiddlewareService, $mdDialog){

    $scope.hardware = hardware;

    MiddlewareService.getAll(function(data){
            $scope.middlewares = data;
        },
        function(err){
            console.log(err)
        });
    $scope.update = function(form){

        if(form.$invalid){
            $scope.formInvalid = true;
        }
        else{
            HardwareService.update($scope.hardware, function(data){
                $mdDialog.hide(data);
            }, function(){

            })
        }

    }


    $scope.delete = function(){

        HardwareService.delete($scope.hardware._id, function(data){
            $mdDialog.hide(true);
        }, function(){

        })
    };

    $scope.close = function () {
        $mdDialog.cancel();
    }
}
