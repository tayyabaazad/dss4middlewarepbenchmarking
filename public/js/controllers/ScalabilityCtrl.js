
'use strict';

angular.module('DSS').controller('ScalabilityController', function($scope, AdminService, $location,ScalabilityService, $mdDialog, $timeout) {

    AdminService.signedInUser(function(data){

        },
        function(err){
            console.log(err)
            $location.path('/')
        });
    ScalabilityService.getAll(function(data){
            console.log(data)
            $scope.scalabilities = data;
        },
        function(err){
            console.log(err)
        });


    $scope.openCreateDialog = function(ev) {
        $mdDialog.show({
            controller: CreateScalabilityController,
            templateUrl: '/views/scalability/create.html',
            targetEvent: ev
        })
            .then(function(createdObj) {
                $scope.scalabilities.push(createdObj)
            }, function() {
            });
    };


    $scope.openUpdateDialog = function(scalability, index, ev) {
        $mdDialog.show({
            controller: UpdateScalabilityController,
            templateUrl: '/views/scalability/update.html',
            locals: {
                scalability : angular.copy(scalability)
            },
            targetEvent: ev
        })
            .then(function(updatedObj) {
                if(updatedObj == true){
                    $scope.scalabilities.splice(index, 1)
                }
                else{
                    $scope.scalabilities[index] = updatedObj;
                }
            }, function() {
            });
    };

});


function CreateScalabilityController ($scope, ScalabilityService,MiddlewareService, $mdDialog){

    $scope.scalability = {};

    MiddlewareService.getAll(function(data){
            $scope.middlewares = data;
        },
        function(err){
            console.log(err)
        });

    $scope.create = function(form){

        if(!form.$valid){
            $scope.formInvalid = true
        }
        else{
            ScalabilityService.create($scope.scalability, function(data){
                $mdDialog.hide(data);
            }, function(){

            })
        }


    }

    $scope.close = function () {
        $mdDialog.cancel();
    }
}


function UpdateScalabilityController ($scope, scalability, ScalabilityService, MiddlewareService, $mdDialog){

    $scope.scalability = scalability;

    MiddlewareService.getAll(function(data){
            $scope.middlewares = data;
        },
        function(err){
            console.log(err)
        });

    $scope.update = function(form){

        if(form.$invalid){
            $scope.formInvalid = true;
        }
        else{
            ScalabilityService.update($scope.scalability, function(data){
                $mdDialog.hide(data);
            }, function(){

            })
        }

    }


    $scope.delete = function(){

        ScalabilityService.delete($scope.scalability._id, function(data){
            $mdDialog.hide(true);
        }, function(){

        })
    };

    $scope.close = function () {
        $mdDialog.cancel();
    }
}
