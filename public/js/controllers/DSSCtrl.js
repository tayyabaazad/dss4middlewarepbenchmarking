'use strict';

angular.module('DSS').controller('DSSController', function($scope, DSS, MiddlewareService, ApplicationScenarioService, BenchmarkService, $mdDialog, $timeout) {


    $scope.lookUpFeature = {
        "application-scenarios" : "Application Scenario",
        "benchmarks" : "Existing Benchmark",
        "hardwares" : "Hardware Requirement",
        "messages" : "Message",
        "message-ratios" : "Message Ratio",
        "messaging-styles" : "Messaging Style",
        "metrics" : "Metric",
        "performances" : "Performance Factor",
        "roles" : "Role",
        "scalabilities" : "Scalability",
        "transactions" : "Transaction",
        "transaction-styles": "Transaction Style",
        "workloads" : "Workload"
    }





    $scope.isArray = function(obj) {
        console.log(angular.isArray(obj))
        return angular.isArray(obj);
    };

    MiddlewareService.getAll(function(data){
            $scope.middlewares = data;
            $scope.lookUpMiddleware = {};

            $scope.middlewares.forEach(function(middleware){
                $scope.lookUpMiddleware[middleware._id]  = middleware   .name
            })

            console.log("Look up middleware")
            console.log($scope.lookUpMiddleware)
    },
    function(err){
        console.log(err)
    });


    ApplicationScenarioService.getAll(function(data){
            console.log("App Scenarios")
            console.log(data)
            $scope.applicationScenarios = data;
        },
        function(err){
            console.log(err)
        });


    BenchmarkService.getAll(function(data){
            console.log("Benchmarks")
            console.log(data)
            $scope.benchmarks = data;
        },
        function(err){
            console.log(err)
        });


    $scope.resetValues = function(){

        $timeout(function(){
            $scope.middlewareSelected = undefined;
            $scope.featureSelected = undefined;
            $scope.appScenarioSelected = undefined;
            $scope.benchmarkSelected = undefined;
            $scope.seleted = undefined;
            $scope.results = undefined;
        }, 1000)



        // using jquery shift screen focus to element with id = start
        $('html, body').animate({ scrollTop: $('#start').offset().top }, 'slow');
    }


    // watch middlewareSelected for changes. When changed, un-select all others
    $scope.$watch("middlewareSelected", function(){

        $scope.featureSelected = undefined;
        $scope.appScenarioSelected = undefined;
        $scope.benchmarkSelected = undefined;
    })
    //DSS.Tags(function(data){
    //        console.log("Tags")
    //        console.log(data)
    //        $scope.tags = data;
    //    },
    //    function(err){
    //        console.log(err)
    //    });
    //
    //$scope.openCreateTagDialog = function(ev) {
    //    $mdDialog.show({
    //        controller: AddTagController,
    //        templateUrl: '/views/dialogs/add.html',
    //        locals: {
    //            url: $scope.lastUsedUrl
    //        },
    //        targetEvent: ev
    //    })
    //        .then(function(tag) {
    //            $scope.tags.push(tag)
    //        }, function() {
    //        });
    //};
    //
    //$scope.searchByTag = function(url){
    //    $scope.lastUsedUrl = url;  // to use it to create tag
    //    DSS.Search(url, function( data){
    //            console.log(data)
    //            $scope.results = data;
    //        },
    //        function(err){
    //            console.log(err)
    //        });
    //}

    $scope.submit = function(){

        // on submit button click

        // start with url ='/api
        var url = "/api";


        //check it middleware is selected and it's noot any. Append it to url
        if($scope.middlewareSelected && $scope.middlewareSelected !== 'any' ){
            url = url + "/middlewares/"+$scope.middlewareSelected
        }

        // feature selected is roles. Append to url accordingly
        if($scope.featureSelected == "roles"){
            if($scope.appScenarioSelected && $scope.appScenarioSelected !== "any"){
                url = "/api/application-scenarios/"+$scope.appScenarioSelected+ "/roles"
            }
            else{
                url = "/api/"+ "roles"
            }
        }

        // feature selected is transaction-styles. Append to url accordingly
        else if($scope.featureSelected == "transaction-styles"){
            if($scope.benchmarkSelected && $scope.benchmarkSelected !== "any"){
                url = "/api/benchmarks/"+ $scope.benchmarkSelected+ "/transaction-styles"
            }
            else{
                url = "/api/"+ "transaction-styles"
            }
        }

        //else check if feature is selected at all
        else if($scope.featureSelected) {
            url = url +"/"+ $scope.featureSelected
        }




        // if nothing is selected assign empty array to results
        if(url == "/api"){
            $scope.results = [];
        }
        else{
            url = url + "?populate=1" //append this query if population is needed. Otherwise reference should come as it is

            //url = url + "&fields[]=model&fields[]=task"
            // call the service function

            if($scope.selected.length){
                $scope.selected.forEach(function(field){
                    url = url + "&fields[]=" + field;
                })
            }
            console.log($scope.selected)
            DSS.Search(url, function( data){
                    console.log(data)
                    $scope.results = data;

                    $scope.featureSelectedCopy = $scope.featureSelected;  // copy to avoid results sentences changing without pressing submit
                    $scope.middlewareSelectedCopy = $scope.middlewareSelected

                    // using jquery shift screen focus to element with id = results
                    $('html, body').animate({ scrollTop: $('#results').offset().top }, 'slow');
                },
                function(err){
                    console.log(err)
                });
        }

        console.log(url)

        //$scope.lastUsedUrl = url;  // to use it to create tag

    }

    $scope.selected = [];
    $scope.toggle = function (item, list) {
        var idx = list.indexOf(item);
        if (idx > -1) {
            list.splice(idx, 1);
        }
        else {
            list.push(item);
        }
    };
    $scope.exists = function (item, list) {
        return list.indexOf(item) > -1;
    };


    $scope.$watch("featureSelected", function(){
        console.log($scope.featureSelected);
        $scope.selected = [];
    })

    $scope.selectedMiddlware = [];

    $scope.toggle = function (item, list) {
        console.log(item)
        var idx = list.indexOf(item);
        if (idx > -1) {
            list.splice(idx, 1);
        }
        else {
            list.push(item);
            //if(item == "MOM"){
            //    $scope.items2 = ["Data Type", "Dimension", "Delivery Mode", "Average Size", "Value", "Destination"]
            //}
            //else if(item == "DBMS"){
            //    $scope.items2 = ["Data Type", "Dimension", "No Of Transactions", "Table Layout"]
            //}
        }
    };
    $scope.exists = function (item, list) {
        return list.indexOf(item) > -1;
    };



    $scope.tags = [
        {
            name: "Benchmark",
            url: "/api/benchmarks?populate=1",
            feature: "benchmarks",
            fields : []
        },
        {
            name: "Benchmark (consortium only)",
            url: "/api/benchmarks?populate=1&fields[]=consortium",
            feature: "benchmarks",
            fields: ["consortium"]
        }];

    $scope.searchByTag = function(tag){
        console.log(tag);
        $scope.middlewareSelected = tag.middlewareSelected;
        $scope.featureSelected = tag.feature;
        $scope.selected = tag.fields;

        DSS.Search(tag.url, function( data){
                console.log(data)
                $scope.results = data;
            },
            function(err){
                console.log(err)
            });

    }
    $scope.items2 = ["Workload", "Number Of Users", "Metric", "Data Type"];
    $scope.selected2 = [];
    $scope.toggle2 = function (item, list) {
        var idx = list.indexOf(item);
        if (idx > -1) list.splice(idx, 1);
        else list.push(item);
    };


});


function AddTagController ($scope, url, DSS, $mdDialog){

    $scope.tag = {};
    $scope.addTag = function(){
        $scope.tag.url = url
        console.log($scope.tag)
        DSS.AddTag($scope.tag, function(data){
            $mdDialog.hide(data);
        }, function(){

        })
    }
    $scope.close = function () {
        $mdDialog.hide();
    }
}
