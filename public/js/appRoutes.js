angular.module('appRoutes', []).config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

	$routeProvider

		// home page
		.when('/', {
			templateUrl: 'views/main.html',
			controller: 'DSSController'
		})

        .when('/items', {
            templateUrl: 'views/applicationScenario/view.html',
            controller: 'DSSController'
        })

        .when('/application-scenarios', {
            templateUrl: 'views/applicationScenario/view.html',
            controller: 'ApplicationScenarioController'
        })

        .when('/benchmarks', {
            templateUrl: 'views/benchmark/view.html',
            controller: 'BenchmarkController'
        })



        .when('/sign-in', {
            templateUrl: 'views/sign-in.html',
            controller: 'SignInController'
        })



        .when('/hardwares', {
            templateUrl: 'views/hardware/view.html',
            controller: 'HardwareController'
        })

        .when('/message-ratios', {
            templateUrl: 'views/messageRatio/view.html',
            controller: 'MessageRatioController'
        })

        .when('/messages', {
            templateUrl: 'views/message/view.html',
            controller: 'MessageController'
        })

        .when('/messaging-styles', {
            templateUrl: 'views/messagingStyle/view.html',
            controller: 'MessagingStyleController'
        })

        .when('/metrics', {
            templateUrl: 'views/metric/view.html',
            controller: 'MetricController'
        })

        .when('/middlewares', {
            templateUrl: 'views/middleware/view.html',
            controller: 'MiddlewareController'
        })

        .when('/performances', {
            templateUrl: 'views/performance/view.html',
            controller: 'PerformanceController'
        })

        .when('/roles', {
            templateUrl: 'views/role/view.html',
            controller: 'RoleController'
        })

        .when('/scalabilities', {
            templateUrl: 'views/scalability/view.html',
            controller: 'ScalabilityController'
        })

        .when('/transactions', {
            templateUrl: 'views/transaction/view.html',
            controller: 'TransactionController'
        })

        .when('/transaction-styles', {
            templateUrl: 'views/transactionStyle/view.html',
            controller: 'TransactionStyleController'
        })

        .when('/workloads', {
            templateUrl: 'views/workload/view.html',
            controller: 'WorkloadController'
        })

        .otherwise({
            redirectTo: '/'
        });

	$locationProvider.html5Mode(true);

}]);