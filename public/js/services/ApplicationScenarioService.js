
'use strict';
angular.module('DSS').factory('ApplicationScenarioService', ['$http', function($http) {

    return {
        getAll: function (success, error) {
            $http.get('/api/application-scenarios').
                success(function (data) {
                    success(data)
                }).
                error(function (data) {
                    error(data);
                });
        },

        create: function (newObj, success, error) {
            $http.post('/api/application-scenarios', newObj).
                success(function (data) {
                    success(data)
                }).
                error(function (data) {
                    error(data);
                });
        },

        update: function (obj, success, error) {
            $http.put('/api/application-scenarios/'+obj._id, obj).
                success(function (data) {
                    success(data)
                }).
                error(function (data) {
                    error(data);
                });
        },

        delete: function (id, success, error) {
            $http.delete('/api/application-scenarios/'+ id).
                success(function (data) {
                    success(data)
                }).
                error(function (data) {
                    error(data);
                });
        }
    }

}]);