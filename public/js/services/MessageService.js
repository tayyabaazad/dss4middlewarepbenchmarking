
'use strict';
angular.module('DSS').factory('MessageService', ['$http', function($http) {

    return {
        getAll: function (success, error) {
            $http.get('/api/messages').
                success(function (data) {
                    success(data)
                }).
                error(function (data) {
                    error(data);
                });
        },

        create: function (newObj, success, error) {
            $http.post('/api/messages', newObj).
                success(function (data) {
                    success(data)
                }).
                error(function (data) {
                    error(data);
                });
        },

        update: function (obj, success, error) {
            $http.put('/api/messages/'+obj._id, obj).
                success(function (data) {
                    success(data)
                }).
                error(function (data) {
                    error(data);
                });
        },

        delete: function (id, success, error) {
            $http.delete('/api/messages/'+ id).
                success(function (data) {
                    success(data)
                }).
                error(function (data) {
                    error(data);
                });
        }
    }

}]);