
'use strict';
angular.module('DSS').factory('ScalabilityService', ['$http', function($http) {

    return {
        getAll: function (success, error) {
            $http.get('/api/scalabilities').
                success(function (data) {
                    success(data)
                }).
                error(function (data) {
                    error(data);
                });
        },

        create: function (newObj, success, error) {
            $http.post('/api/scalabilities', newObj).
                success(function (data) {
                    success(data)
                }).
                error(function (data) {
                    error(data);
                });
        },

        update: function (obj, success, error) {
            $http.put('/api/scalabilities/'+obj._id, obj   ).
                success(function (data) {
                    success(data)
                }).
                error(function (data) {
                    error(data);
                });
        },

        delete: function (id, success, error) {
            $http.delete('/api/scalabilities/'+ id).
                success(function (data) {
                    success(data)
                }).
                error(function (data) {
                    error(data);
                });
        }
    }

}]);