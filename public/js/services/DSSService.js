'use strict';
angular.module('DSS').factory('DSS', ['$http', function($http) {

    return {

        Search: function (url, success, error) {
            $http.get(url).
                success(function (data) {
                    success(data)
                }).
                error(function (data) {
                    error(data);
                });
        }

        // removed the tag feature for now
        //AddTag: function (data, success, error) {
        //    $http.post('/tags', data).
        //        success(function (data) {
        //            success(data)
        //        }).
        //        error(function (data) {
        //            error(data);
        //        });
        //},
        //Tags: function (success, error) {
        //    $http.get('/tags').
        //        success(function (data) {
        //            success(data)
        //        }).
        //        error(function (data) {
        //            error(data);
        //        });
        //}
    }

}]);