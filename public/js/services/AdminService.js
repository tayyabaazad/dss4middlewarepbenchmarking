
'use strict';
angular.module('DSS').factory('AdminService', ['$http', function($http) {

    return {
        signedInUser: function (success, error) {
            $http.get('/signed-in-user').
                success(function (data) {
                    success(data)
                }).
                error(function (data) {
                    error(data);
                });
        },
        signOut: function (success, error) {
            $http.get('/sign-out').
                success(function (data) {
                    success(data)
                }).
                error(function (data) {
                    error(data);
                });
        },

        signIn: function (user, success, error) {
            $http.post('/sign-in', user).
                success(function (data) {
                    success(data)
                }).
                error(function (data) {
                    error(data);
                });
        }
    }

}]);