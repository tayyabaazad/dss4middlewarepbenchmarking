
'use strict';
angular.module('DSS').factory('WorkloadService', ['$http', function($http) {

    return {
        getAll: function (success, error) {
            $http.get('/api/workloads').
                success(function (data) {
                    success(data)
                }).
                error(function (data) {
                    error(data);
                });
        },

        create: function (newObj, success, error) {
            $http.post('/api/workloads', newObj).
                success(function (data) {
                    success(data)
                }).
                error(function (data) {
                    error(data);
                });
        },

        update: function (obj, success, error) {
            $http.put('/api/workloads/'+obj._id, obj).
                success(function (data) {
                    success(data)
                }).
                error(function (data) {
                    error(data);
                });
        },

        delete: function (id, success, error) {
            $http.delete('/api/workloads/'+ id).
                success(function (data) {
                    success(data)
                }).
                error(function (data) {
                    error(data);
                });
        }
    }

}]);