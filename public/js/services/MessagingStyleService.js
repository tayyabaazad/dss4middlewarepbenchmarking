
'use strict';
angular.module('DSS').factory('MessagingStyleService', ['$http', function($http) {

    return {
        getAll: function (success, error) {
            $http.get('/api/messaging-styles').
                success(function (data) {
                    success(data)
                }).
                error(function (data) {
                    error(data);
                });
        },

        create: function (newObj, success, error) {
            $http.post('/api/messaging-styles', newObj).
                success(function (data) {
                    success(data)
                }).
                error(function (data) {
                    error(data);
                });
        },

        update: function (obj, success, error) {
            $http.put('/api/messaging-styles/'+obj._id, obj).
                success(function (data) {
                    success(data)
                }).
                error(function (data) {
                    error(data);
                });
        },

        delete: function (id, success, error) {
            $http.delete('/api/messaging-styles/'+ id).
                success(function (data) {
                    success(data)
                }).
                error(function (data) {
                    error(data);
                });
        }
    }

}]);