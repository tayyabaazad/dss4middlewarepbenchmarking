var mongoose       = require('mongoose');  // Include mongoose library
var db = require('./config/db');  //get settings from config.json file fo db
mongoose.connect(db.url);  // connect to mongodb



// Get all the mongoose models and assign them to variables
var Benchmark = require('./app/models/Benchmark');
var Middleware = require('./app/models/Middleware');
var Workload = require('./app/models/Workload');
var Metric = require('./app/models/Metric');
var ApplicationScenario = require('./app/models/ApplicationScenario');
var Role = require('./app/models/Role');
var Hardware = require('./app/models/Hardware');
var Performance = require('./app/models/Performance');
var Message = require('./app/models/Message');
var MessagingStyle = require('./app/models/MessagingStyle');
var MessageRatio = require('./app/models/MessageRatio');
var Scalability = require('./app/models/Scalability');
var Transaction = require('./app/models/Transaction');
var TransactionStyle = require('./app/models/TransactionStyle');



console.log("***** Welcome to the seed file *****")
console.log("Creating documents to seed...")


// In the followings, create an array (plural name). Each element is the instance of the above defined model.



var middlewares = [
    new Middleware ({
        name: "Java Server"
    }),
    new Middleware ({
        name: "DBMS"
    }),
    new Middleware ({
        name: "Message Oriented Middleware (MOM)"
    })
]


var benchmarks = [
    new Benchmark({
        consortium: "TPC",
        name: "TPC-C",
        type: "Random & expressive",
        middleware: middlewares[1]._id
    }),
    new Benchmark({
        consortium: "TPC",
        name: "TPC-E",
        type: "Realistic & complex",
        middleware: middlewares[1]._id
    }),
    new Benchmark({
        consortium: "SPEC",
        name: "Specjms2007",
        middleware: middlewares[2]._id
    }),
    new Benchmark({
        consortium: "SPEC",
        name: "Specjbb2015",
        middleware: middlewares[0]._id
    })
];


var workloads = [
    new Workload({
        middleware: middlewares[2]._id,
        type: ["SPECjms2007@horizontal", "SPECjms2007@vertical"],
        measure: ["Message injection rate"],
        computation: "Tc = -ln(x) * Tm\nwhere:\nTc = Cycle Time\nTm = Mean desired cycle time\nln = natural log (base e)\nx = random number, with at least 31 bits of precision, from a uniform distribution such that (0 < x <= 1)"  // use http://www.free-online.tools/en/text/remove_line_breaks.aspx to remove new lines and replace them with \n
    }),
    new Workload({
        middleware: middlewares[0]._id,
        type: ["Max-jOPS", "Critical-jOPS"],
        measure: ["Injection rate"]
    }),
    new Workload({
        middleware: middlewares[1]._id,
        type: ["TpmC", "tpsE"],
        measure: ["Transaction-per-minute", "Transaction-per-second"]
    })
]



var metrics = [
    new Metric ({
        middleware: middlewares[0]._id,
        type: "Throughput",
        name: "Max-jOPS"
    }),


    new Metric ({
        middleware: middlewares[0]._id,
        type: "Response time",
        name: "Critical-jOPS",
        additionalInfo: "Points for the response time: 10ms, 25ms, 50ms, 75ms and 100ms"
    }),
    new Metric ({
        middleware: middlewares[2]._id
    }),
    new Metric ({
        middleware: middlewares[1]._id,
        type: "Energy efficient",
        name: "watts/tpsE"
    }),
    new Metric ({
        middleware: middlewares[1]._id,
        type: "Throughput"
    }),

    new Metric ({
        middleware: middlewares[1]._id,
        type: "Response Time",
        name: "watts/tpsE",
        additionalInfo: "RTn= eTn-sTn\n\nwhere:\nsTn and eTn are measured at the Driver;\n\nsTn= time measured before the first byte of input data of t\nhe Transaction is sent by the Driverto the SUT;\n\n and\n\neTn=time measured after the last byte of output data from the \nTransaction is received by the Driver from the \nSUT.\n"
    })
]


var applicationScenarios = [
    new ApplicationScenario({
        middleware: middlewares[0]._id,
        model: "Supermarket company",
        task: "\nPOS(Point Of Sales) in local Supermarkets(SM) as well as online purchases, \n\nIssuing and managing coupons/discounts and customer payments \n\nManaging receipts, invoices and user database in the company Headquarters (HQ) \n\nInteraction with Suppliers (SP) for replenishing the inventory \n\nData mining(DM) operations in the company Headquarters to identify sale patterns and generating quarterly business reports\n"
    }),
    new ApplicationScenario({
        middleware: middlewares[1]._id,
        model: "Wholesale Supplier",
        task: "The Company portrayed by the benchmark is a wholesale supplier with a number of geographically distributed \nsales districts and associated warehouses. As the Company's business expands, new warehouses and associated \nsales districts are created . Each regional warehouse covers 10 districts. Each district serves 3,000 customers. All \nwarehouses maintain stocks for the 100,000 items sold by the Company. The following diagram illustrates the \nwarehouse, district, and customer hierarchy of TPC-C's business environment."
    }),

    new ApplicationScenario({
        middleware: middlewares[1]._id,
        model: "Brokerage Firm",
        task: "The Company portrayed by the benchmark is a wholesale supplier with a number of geographically distributed \nsales districts and associated warehouses. As the Company's business expands, new warehouses and associated \nsales districts are created . Each regional warehouse covers 10 districts. Each district serves 3,000 customers. All \nwarehouses maintain stocks for the 100,000 items sold by the Company. The following diagram illustrates the \nwarehouse, district, and customer hierarchy of TPC-C's business environment."
    }),

    new ApplicationScenario({
        middleware: middlewares[2]._id,
        model: "Supply Chain",
        task: "The following Interactions are part of the scenario:\n1. Order/shipment handling between Supermarkets and Distribution Centers.\n2. Purchase order/shipment handling between Distribution Centers and Suppliers.\n3. Price updates sent by Headquarters to the Supermarkets.\n4. Supermarket inventory management.\n5. Sales statistics collection.\n6. New product announcements.\n7. Distribution of credit card hot lists.\n"
    })
]



var roles = [
    new Role({
        type:"Admin",
        number: 1,
        name: ["Headquarters (HQ)"],
        applicationScenario: applicationScenarios[0]._id
    }),
    new Role({
        type:"User",
        number: 4,
        name: ["Supermarkets (SM)", "Suppliers (SP)"],
        applicationScenario: applicationScenarios[0]._id
    }),
    new Role({
        type:"Admin",
        number: 1,
        name: ["Headquarters (HQ)"],
        applicationScenario: applicationScenarios[3]._id
    }),
    new Role({
        type:"User",
        number: 3,
        name: ["Distribution centers (DCs)", "Supermarkets (SMs)", "Suppliers (SPs)"],
        applicationScenario: applicationScenarios[3]._id
    }),
    new Role({
        type:"Admin",
        number: 1,
        name: ["Company"],
        applicationScenario: applicationScenarios[1]._id
    }),
    new Role({
        type:"User",
        number: 3,
        name: ["Warehouse", "District", "Customer"],
        applicationScenario: applicationScenarios[1]._id
    }),
    new Role({
        type:"User",
        number: 3,
        name: ["Customer", "brokerage", "market"],
        applicationScenario: applicationScenarios[2]._id
    }),
    new Role({
        type:"Admin",
        number: 1,
        name: ["Dimension"],
        applicationScenario: applicationScenarios[2]._id
    })
]




var hardwares = [
    new Hardware ({
        middleware: middlewares[2]._id,
        minRequirement: "one or more client machines are required, as well as the network equipment to connect the clients to the server"
    }),

    new Hardware ({
        middleware: middlewares[0]._id,
        minRequirement: "At minimum, a server with 8GB of Ram is required. 24G and above is recommended for performance runs"
    })
]


var performances = [
    new Performance ({
        middleware: middlewares[2]._id,
        factors: ["The hardware config", "JMS Server Software", "JWM software", "network performance"]
    }),
    new Performance ({
        middleware: middlewares[0]._id,
        factors: ["Number of processors and processor characteristics", "memory subsystem", "operating system capabilities", "java runtime environment"]
    })

]


var messages = [
    new Message({
        middleware: middlewares[2]._id,
        dataType: ["TextMessage", "ObjectMessage", "StreamMessage", "MapMessage"],
        dimension: ["Point-to-point (P2P)", "publish/subscribe (Pub/sub)"],
        deliveryMode: ["Persistent/non-persistent", "durable/non-durable", "transactional/non-transactional"],
        avgSizeMin: ["0 kbytes", "24 kbytes"],
        avgSizeMax: ["5 kbytes", "27 kbytes"],
        value: ["50% P2P messages and 50% pub/sub", "50% P2P messages persistent 50% non-persistent", "25% pub/sub messages persistent, 75% non-persistent"],
        destinationType: ["Queue", "topic"]

    })
]


var messagingStyles = [
    new MessagingStyle({
        middleware: middlewares[2]._id,
        dimension: "Point-to-point",
        size: "large",
        deliveryMode: ["Persistent", "Transactional"]

    }),
    new MessagingStyle({
        middleware: middlewares[2]._id,
        dimension: "Point-to-point",
        size: "large",
        deliveryMode: ["Non-persistent", "non-transactional"]

    }),
    new MessagingStyle({
        middleware: middlewares[2]._id,
        dimension: "Publish/subscribe",
        size: "small",
        deliveryMode: ["Persistent", "transactional", "durable"]

    }),
    new MessagingStyle({
        middleware: middlewares[2]._id,
        dimension: "Publish/subscribe",
        size: "small",
        deliveryMode: ["Non-persistent", "Non-transactional", "Non-durable"]

    })

]




var messageRatios = [
    new MessageRatio({
        middleware: middlewares[2]._id,
        messagingType: "Transactional and Point-to-point",
        numberMessaging: "25.55%",
        numberBytes: "23.74%"
    }),
    new MessageRatio({
        middleware: middlewares[2]._id,
        messagingType: "Transactional and Publish/subscribe",
        numberMessaging: "12.45%",
        numberBytes: "2.41%"
    }),
    new MessageRatio({
        middleware: middlewares[2]._id,
        messagingType: "Non-transactional and Point-to-point",
        numberMessaging: "24.55%",
        numberBytes: "49.19%"
    }),
    new MessageRatio({
        middleware: middlewares[2]._id,
        messagingType: "Non-transactional and Publish/subscribe",
        numberMessaging: "37.46%",
        numberBytes: "24.66%"
    }),
]


var scalabilities = [
    new Scalability({
        middleware: middlewares[2]._id,
        name: "horizontal",
        description: "Workload is scaled by increasing the number of destinations (locations), while keeping the traffic (message count) per destination constant",
        trafficType: "constant"
    }),
    new Scalability({
        middleware: middlewares[2]._id,
        name: "vertical",
        description: "By increasing the message count (traffic) per entity, while keeping the number of entities constant",
        trafficType: "dynamic"
    })
]




var transactions = [
    new Transaction({
        middleware: middlewares[1]._id,
        dataType: ["UID", "Char", "NUM", "DATE"],
        dimension: ["Read-only", "write-only", "read-write"],
        type: "transactions",
        numberMin: "5 transactions",
        numberMax: "10 transactions",
        tableLayout: ["N unique IDs", "variable text size N", "fixed text size N", "date and time", "numeric (m [,n])", "signed  numeric (m [,n]), null"]
    })
]




var transactionStyles = [
    new TransactionStyle({
        middleware: middlewares[1]._id,
        property: "Read intensive",
        benchmark: benchmarks[1]._id,
        value: "23.1% of read-write transaction types and 76.9% of read-only transaction types"
    }),
    new TransactionStyle({
        middleware: middlewares[1]._id,
        property: "Write intensive",
        benchmark: benchmarks[0]._id,
        value: "92% of read-write transaction types and 8% of read-only transaction types"
    })
]











console.log("Putting the created documents in mongodDB instance ...");
var insertionCount = 0, totalCollections = 14;


// In the followings, pass the above arrays to the respective model's create function. This will create all the documents of that model. Print Success message

Benchmark.create(benchmarks, function(err){
    if(err){
        throw err
    }
    else{
        console.log("Benchmarks Successfully Created!");
        insertionCount ++;
        exitCheck()
    }
});

Middleware.create(middlewares, function(err){
    if(err){
        throw err
    }
    else{
        console.log("Middlewares Successfully Created!");
        insertionCount ++;
        exitCheck()

    }
});


Workload.create(workloads, function(err){
    if(err){
        throw err
    }
    else{
        console.log("Workloads Successfully Created!");
        insertionCount ++;
        exitCheck()

    }
});


Metric.create(metrics, function(err){
    if(err){
        throw err
    }
    else{
        console.log("Metrics Successfully Created!");
        insertionCount ++;
        exitCheck()

    }
});


ApplicationScenario.create(applicationScenarios, function(err){
    if(err){
        throw err
    }
    else{
        console.log("Application Scenarios Successfully Created!");
        insertionCount ++;
        exitCheck()

    }
});


Role.create(roles, function(err){
    if(err){
        throw err
    }
    else{
        console.log("Roles Successfully Created!");
        insertionCount ++;
        exitCheck()

    }
});

Hardware.create(hardwares, function(err){
    if(err){
        throw err
    }
    else{
        console.log("Hardwares Successfully Created!");
        insertionCount ++;
        exitCheck()

    }
});


Performance.create(performances, function(err){
    if(err){
        throw err
    }
    else{
        console.log("Performances Successfully Created!");
        insertionCount ++;
        exitCheck()

    }
});


Message.create(messages, function(err){
    if(err){
        throw err
    }
    else{
        console.log("Messages Successfully Created!");
        insertionCount ++;
        exitCheck()

    }
});


MessagingStyle.create(messagingStyles, function(err){
    if(err){
        throw err
    }
    else{
        console.log("Messaging Styles Successfully Created!");
        insertionCount ++;
        exitCheck()

    }
});


MessageRatio.create(messageRatios, function(err){
    if(err){
        throw err
    }
    else{
        console.log("Roles Successfully Created!");
        insertionCount ++;
        exitCheck()

    }
});


Scalability.create(scalabilities, function(err){
    if(err){
        throw err
    }
    else{
        console.log("Scalabilities Successfully Created!");
        insertionCount ++;
        exitCheck()

    }
});


Transaction.create(transactions, function(err){
    if(err){
        throw err
    }
    else{
        console.log("Transactions Successfully Created!");
        insertionCount ++;
        exitCheck()

    }
});


TransactionStyle.create(transactionStyles, function(err){
    if(err){
        throw err
    }
    else{
        console.log("Transaction Styles Successfully Created!");
        insertionCount ++;
        exitCheck()

    }
});


// Check if all collections have been placed in database. If so, exit (stop the script)
function exitCheck(){
    if(insertionCount == totalCollections){
        console.log("*****");
        console.log("All documents saved to mongodb successfully!!!");
        console.log("*****");
        process.exit(1)  // Seeding Complete!!!
    }
}


